package com.senla.autoservice.view.action.master;

import com.senla.autoservice.exceptions.EntityDeleteException;
import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingMaster;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingMasterNameForDelete;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutDeleting;

public class DeleteMasterAction implements Action {

    private final Scanner scanner;
    private final MasterService masterService;

    public DeleteMasterAction() {
        this.scanner = ScannerFactory.getScanner();
        this.masterService = ServiceFactory.getMasterService();
    }

    @Override
    public void execute() {
        printMessageAboutChoosingMasterNameForDelete();

        Master deletableMaster = masterService.readByName(scanner.next());
        printLine();

        if (deletableMaster == null) {
            printErrorAboutNotFoundingMaster();
            return;
        }

        try {
            if (masterService.delete(deletableMaster.getId())) {
                printMessageAboutDeleting("Мастер");
            }
        } catch (EntityNotFoundException | EntityDeleteException e) {
            System.err.println(e.getMessage());
        }

    }
}
