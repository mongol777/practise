package com.senla.autoservice.view.action.place;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingGarage;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageForCreate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingPlaceNumber;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreating;

public class CreatePlaceAction implements Action {
    private final Scanner scanner;
    private final PlaceService placeService;
    private final GarageService garageService;

    public CreatePlaceAction() {
        this.scanner = ScannerFactory.getScanner();
        this.placeService = ServiceFactory.getPlaceService();
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        Garage garage;
        try {
            garage = chooseGarage();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (garage == null) {
            printErrorAboutNotFoundingGarage();
            return;
        }

        int number;
        try {
            number = createNumber();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (placeService.save(new Place(number, garage))) {
            printMessageAboutCreating("Место");
        }

        printLine();
    }

    private int createNumber() {
        printMessageAboutChoosingPlaceNumber();
        printLine();
        return scanner.nextInt();
    }

    private Garage chooseGarage() {
        printMessageAboutChoosingGarageForCreate(garageService, "место");
        return garageService.readByNumber(scanner.nextInt());
    }
}
