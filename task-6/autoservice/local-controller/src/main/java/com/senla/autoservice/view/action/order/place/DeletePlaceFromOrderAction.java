package com.senla.autoservice.view.action.order.place;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingOrder;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForDeletePlaceFromOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutDeletingPlaceFromOrder;

public class DeletePlaceFromOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;

    public DeletePlaceFromOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
    }

    @Override
    public void execute() {
        Order order;
        try {
            order = findOrderForDeletePlace();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (order == null) {
            printErrorAboutNotFoundingOrder();
            return;
        }

        order.setPlace(null);

        if (orderService.save(order)) {
            printMessageAboutDeletingPlaceFromOrder();
        }
        printLine();
    }

    private Order findOrderForDeletePlace() {
        printMessageAboutChoosingOrderNumberForDeletePlaceFromOrder();
        Order order = orderService.readByNumber(scanner.nextInt());
        printLine();
        return order;
    }
}
