package com.senla.autoservice.view;

import com.senla.autoservice.view.action.download.DownloadAction;
import com.senla.autoservice.view.action.garage.CreateGarageAction;
import com.senla.autoservice.view.action.garage.DeleteGarageAction;
import com.senla.autoservice.view.action.garage.ReadAllGarageAction;
import com.senla.autoservice.view.action.master.CreateMasterAction;
import com.senla.autoservice.view.action.master.DeleteMasterAction;
import com.senla.autoservice.view.action.master.ReadAllMasterAction;
import com.senla.autoservice.view.action.master.SortMasterAction;
import com.senla.autoservice.view.action.master.UpdateMasterAction;
import com.senla.autoservice.view.action.order.CreateOrderAction;
import com.senla.autoservice.view.action.order.DeleteOrderAction;
import com.senla.autoservice.view.action.order.ReadAllOrderAction;
import com.senla.autoservice.view.action.order.SortOrderAction;
import com.senla.autoservice.view.action.order.UpdateOrderAction;
import com.senla.autoservice.view.action.order.master.AddMasterToOrderAction;
import com.senla.autoservice.view.action.order.master.DeleteMasterFromOrderAction;
import com.senla.autoservice.view.action.order.place.AddPlaceToOrderAction;
import com.senla.autoservice.view.action.order.place.DeletePlaceFromOrderAction;
import com.senla.autoservice.view.action.place.CreatePlaceAction;
import com.senla.autoservice.view.action.place.DeletePlaceAction;
import com.senla.autoservice.view.action.place.ReadAllPlaceAction;
import com.senla.autoservice.view.action.place.UpdatePlaceAction;
import com.senla.autoservice.view.action.save.SaveAction;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MenuBuilder {
    private Menu rootMenu;

    public void buildMenu() {
        rootMenu = new Menu("Главное меню", new ArrayList<>());

        buildGarageMenu(rootMenu);
        buildMasterMenu(rootMenu);
        buildPlaceMenu(rootMenu);
        buildOrderMenu(rootMenu);
        buildSaveMenu(rootMenu);
        buildDownloadMenu(rootMenu);
    }

    private void buildGarageMenu(Menu menu) {
        List<MenuItem> garageItems = new ArrayList<>();

        garageItems.add(new MenuItem("Создать гараж", new CreateGarageAction(), null));
        garageItems.add(new MenuItem("Все гаражи", new ReadAllGarageAction(), null));
        garageItems.add(new MenuItem("Удалить гараж", new DeleteGarageAction(), null));

        menu.getItems().add(new MenuItem("Меню гаражей", null, new Menu("Меню гаражей", garageItems)));
    }

    private void buildMasterMenu(Menu menu) {
        List<MenuItem> masterItems = new ArrayList<>();

        masterItems.add(new MenuItem("Создать мастера", new CreateMasterAction(), null));
        masterItems.add(new MenuItem("Все мастера", new ReadAllMasterAction(), null));
        masterItems.add(new MenuItem("Обновить мастера", new UpdateMasterAction(), null));
        masterItems.add(new MenuItem("Удалить мастера", new DeleteMasterAction(), null));
        masterItems.add(new MenuItem("Сортировка мастеров", new SortMasterAction(), null));

        menu.getItems().add(new MenuItem("Меню мастеров", null, new Menu("Меню мастеров", masterItems)));
    }

    private void buildPlaceMenu(Menu menu) {
        List<MenuItem> placeItems = new ArrayList<>();

        placeItems.add(new MenuItem("Создать место", new CreatePlaceAction(), null));
        placeItems.add(new MenuItem("Все места", new ReadAllPlaceAction(), null));
        placeItems.add(new MenuItem("Обновить место", new UpdatePlaceAction(), null));
        placeItems.add(new MenuItem("Удалить место", new DeletePlaceAction(), null));

        menu.getItems().add(new MenuItem("Меню мест", null, new Menu("Меню мест", placeItems)));
    }

    private void buildOrderMenu(Menu menu) {
        List<MenuItem> orderItems = new ArrayList<>();

        orderItems.add(new MenuItem("Создать заказ", new CreateOrderAction(), null));
        orderItems.add(new MenuItem("Все заказы", new ReadAllOrderAction(), null));
        orderItems.add(new MenuItem("Обновить заказ", new UpdateOrderAction(), null));
        orderItems.add(new MenuItem("Удалить заказ", new DeleteOrderAction(), null));
        orderItems.add(new MenuItem("Сортировка заказов", new SortOrderAction(), null));
        orderItems.add(new MenuItem("Добавление мастера", new AddMasterToOrderAction(), null));
        orderItems.add(new MenuItem("Удаление мастера", new DeleteMasterFromOrderAction(), null));
        orderItems.add(new MenuItem("Добавление места", new AddPlaceToOrderAction(), null));
        orderItems.add(new MenuItem("Удаление места", new DeletePlaceFromOrderAction(), null));

        menu.getItems().add(new MenuItem("Меню заказов", null, new Menu("Меню заказов", orderItems)));
    }

    private void buildSaveMenu(Menu menu) {
        List<MenuItem> saveItems = new ArrayList<>();

        saveItems.add(new MenuItem("Сохранить данные в файл", new SaveAction(), null));

        menu.getItems().add(new MenuItem("Меню сохранения", null, new Menu("Меню сохранения", saveItems)));
    }

    private void buildDownloadMenu(Menu menu) {
        List<MenuItem> downloadItems = new ArrayList<>();

        downloadItems.add(new MenuItem("Загрузить данные из файла", new DownloadAction(), null));

        menu.getItems().add(new MenuItem("Меню загрузки", null, new Menu("Меню загрузки", downloadItems)));
    }
}
