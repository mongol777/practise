package com.senla.autoservice.view.action.garage;

import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.view.action.Action;

import static com.senla.autoservice.writer.MessageWriter.printLine;

public class ReadAllGarageAction implements Action {
    private final GarageService garageService;

    public ReadAllGarageAction() {
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        garageService.readAll().forEach(System.out::println);
        printLine();
    }
}
