package com.senla.autoservice.view;

import com.senla.autoservice.factory.scanner.ScannerFactory;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectMenuNumber;

public class MenuController {
    private final Scanner scanner;
    private final MenuNavigator navigator;
    private final MenuBuilder builder;

    public MenuController(MenuNavigator navigator, MenuBuilder builder) {
        this.scanner = ScannerFactory.getScanner();
        this.navigator = navigator;
        this.builder = builder;
    }

    public void run() {
        builder.buildMenu();
        navigator.setCurrentMenu(builder.getRootMenu());

        while (true) {
            navigator.printMenu();
            String request = scanner.nextLine();

            try {
                int number = Integer.parseInt(request) - 1;
                navigator.navigate(number);
            } catch (NumberFormatException ex) {
                switch (request) {
                    case "root":
                        navigator.setCurrentMenu(builder.getRootMenu());
                        continue;
                    case "exit":
                        return;
                    default:
                        printErrorAboutIncorrectMenuNumber();
                }
            }
        }
    }
}
