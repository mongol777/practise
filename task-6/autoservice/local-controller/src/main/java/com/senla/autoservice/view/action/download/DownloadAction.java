package com.senla.autoservice.view.action.download;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.order.status.OrderStatus;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.models.status.EmploymentStatus;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotDownloading;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotExistingFile;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutSuccessfulDownloading;

public class DownloadAction implements Action {
    private final GarageService garageService;
    private final MasterService masterService;
    private final PlaceService placeService;
    private final OrderService orderService;

    private final File garageFile;
    private final File masterFile;
    private final File placeFile;
    private final File orderFile;

    public DownloadAction() {
        this.garageService = ServiceFactory.getGarageService();
        this.masterService = ServiceFactory.getMasterService();
        this.placeService = ServiceFactory.getPlaceService();
        this.orderService = ServiceFactory.getOrderService();

        this.garageFile = new File("local-controller/src/main/resources/garages.csv");
        this.masterFile = new File("local-controller/src/main/resources/masters.csv");
        this.placeFile = new File("local-controller/src/main/resources/places.csv");
        this.orderFile = new File("local-controller/src/main/resources/orders.csv");
    }

    @Override
    public void execute() {
        System.out.println("Загрузка всех данных из CSV-файла");

        downloadGarages(garageFile);
        downloadMasters(masterFile);
        downloadPlaces(placeFile);
        downloadOrders(orderFile);
    }

    private void downloadGarages(File file) {
        CSVParser parser = new CSVParserBuilder().withEscapeChar('/').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withCSVParser(parser).withSkipLines(1).build()) {
            if (file.exists()) {
                List<String[]> garagesStateString = reader.readAll();

                for (String[] garageStateString : garagesStateString) {
                    long garageId = Long.parseLong(garageStateString[0]);
                    int garageNumber = Integer.parseInt(garageStateString[1]);

                    Garage garage = new Garage(garageId, garageNumber);

                    garageService.save(garage);
                }

                printMessageAboutSuccessfulDownloading("гаражей");
            } else {
                printErrorAboutNotExistingFile();
            }

        } catch (IOException | CsvException ex) {
            printErrorAboutNotDownloading("гаражи");
        }
    }

    private void downloadMasters(File file) {
        CSVParser parser = new CSVParserBuilder().withEscapeChar('/').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withCSVParser(parser).withSkipLines(1).build()) {
            if (file.exists()) {
                List<String[]> mastersStateString = reader.readAll();

                for (String[] masterStateString : mastersStateString) {
                    long masterId = Long.parseLong(masterStateString[0]);
                    String masterName = masterStateString[1];

                    EmploymentStatus masterStatus = null;
                    switch (masterStateString[2]) {
                        case "FREE":
                            masterStatus = EmploymentStatus.FREE;
                            break;
                        case "BUSY":
                            masterStatus = EmploymentStatus.BUSY;
                            break;
                    }

                    Garage masterGarage = garageService.readById(Long.parseLong(masterStateString[3]));

                    if (masterId > 0 && masterName != null && masterStatus != null && masterGarage != null) {
                        Master master = new Master(masterId, masterName, masterStatus, masterGarage);
                        masterService.save(master);
                    }
                }

                printMessageAboutSuccessfulDownloading("мастеров");

            } else {
                printErrorAboutNotExistingFile();
            }
        } catch (IOException | CsvException ex) {
            printErrorAboutNotDownloading("мастеров");
        }
    }

    private void downloadPlaces(File file) {
        CSVParser parser = new CSVParserBuilder().withEscapeChar('/').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withCSVParser(parser).withSkipLines(1).build()) {
            if (file.exists()) {
                List<String[]> placesStateString = reader.readAll();

                for (String[] placeStateString : placesStateString) {
                    long placeId = Long.parseLong(placeStateString[0]);
                    int placeNumber = Integer.parseInt(placeStateString[1]);

                    EmploymentStatus placeStatus = null;
                    switch (placeStateString[2]) {
                        case "FREE":
                            placeStatus = EmploymentStatus.FREE;
                            break;
                        case "BUSY":
                            placeStatus = EmploymentStatus.BUSY;
                            break;
                    }

                    Garage placeGarage = garageService.readById(Long.parseLong(placeStateString[3]));

                    if (placeId > 0 && placeNumber > 0 && placeStatus != null && placeGarage != null) {
                        Place place = new Place(placeId, placeNumber, placeStatus, placeGarage);
                        placeService.save(place);
                    }
                }

                printMessageAboutSuccessfulDownloading("мест");

            } else {
                printErrorAboutNotExistingFile();
            }
        } catch (IOException | CsvException ex) {
            printErrorAboutNotDownloading("места");
        }
    }

    private void downloadOrders(File file) {
        CSVParser parser = new CSVParserBuilder().withEscapeChar('/').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withCSVParser(parser).withSkipLines(1).build()) {
            if (file.exists()) {
                List<String[]> ordersStateString = reader.readAll();

                for (String[] orderStateString : ordersStateString) {
                    long orderId = Long.parseLong(orderStateString[0]);
                    int orderNumber = Integer.parseInt(orderStateString[1]);

                    LocalDate orderDateOfReceiving = LocalDate.parse(orderStateString[2]);
                    LocalDate orderDateOfExpiration = LocalDate.parse(orderStateString[3]);
                    LocalDate orderDateOfStart = LocalDate.parse(orderStateString[4]);
                    LocalDate orderDateOfEnd = LocalDate.parse(orderStateString[5]);

                    double orderPrice = Double.parseDouble(orderStateString[6]);

                    OrderStatus orderStatus = null;
                    switch (orderStateString[7]) {
                        case "IN_PROGRESS":
                            orderStatus = OrderStatus.IN_PROGRESS;
                            break;
                        case "CANCELLED":
                            orderStatus = OrderStatus.CANCELED;
                            break;
                        case "DELETED":
                            orderStatus = OrderStatus.DELETED;
                            break;
                        case "DONE":
                            orderStatus = OrderStatus.DONE;
                            break;
                    }

                    Set<Master> orderMasters = getOrderMasters(orderStateString[8]);

                    Place orderPlace = placeService.readById(Long.parseLong(orderStateString[9]));

                    Garage orderGarage = garageService.readById(Long.parseLong(orderStateString[10]));

                    if (orderId > 0 &&
                            orderNumber > 0 &&
                            orderDateOfReceiving != null &&
                            orderDateOfExpiration != null &&
                            orderDateOfStart != null &&
                            orderDateOfEnd != null &&
                            orderPrice > 0 &&
                            orderStatus != null &&
                            orderGarage != null)
                    {
                        Order order = new Order(orderId,
                                orderNumber,
                                orderDateOfReceiving,
                                orderDateOfExpiration,
                                orderDateOfStart,
                                orderDateOfEnd,
                                orderPrice,
                                orderStatus,
                                orderMasters,
                                orderPlace,
                                orderGarage);
                        orderService.save(order);
                    }
                }

                printMessageAboutSuccessfulDownloading("заказов");

            } else {
                printErrorAboutNotExistingFile();
            }
        } catch (IOException | CsvException ex) {
            printErrorAboutNotDownloading("заказы");
        }
    }

    private Set<Master> getOrderMasters(String orderMastersId) {
        Set<Master> orderMasters = new HashSet<>();

        String[] mastersIdString = orderMastersId.split(";");

        for (String masterIdString : mastersIdString) {
            Master orderMaster = masterService.readById(Long.parseLong(masterIdString));
            if (orderMaster != null) {
                orderMasters.add(orderMaster);
            }
        }

        return orderMasters;
    }


}
