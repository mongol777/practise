package com.senla.autoservice.view.action.save;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotSaving;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutSuccessfulSaving;

public class SaveAction implements Action {
    private final GarageService garageService;
    private final MasterService masterService;
    private final PlaceService placeService;
    private final OrderService orderService;

    private final File garageFile;
    private final File masterFile;
    private final File placeFile;
    private final File orderFile;

    public SaveAction() {
        this.masterService = ServiceFactory.getMasterService();
        this.placeService = ServiceFactory.getPlaceService();
        this.orderService = ServiceFactory.getOrderService();
        this.garageService = ServiceFactory.getGarageService();

        this.garageFile = new File("local-controller/src/main/resources/garages.csv");
        this.masterFile = new File("local-controller/src/main/resources/masters.csv");
        this.placeFile = new File("local-controller/src/main/resources/places.csv");
        this.orderFile = new File("local-controller/src/main/resources/orders.csv");
    }

    @Override
    public void execute() {
        System.out.println("Сохранение всех данных в CSV-файл");

        saveGarages(garageFile);
        saveMasters(masterFile);
        savePlaces(placeFile);
        saveOrders(orderFile);
    }

    private void saveGarages(File file) {
        try (CSVWriter writer = new CSVWriter(new FileWriter(file), ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER, '/', ICSVWriter.DEFAULT_LINE_END))
        {
            if (file.exists()) {
                file.delete();
            }

            writer.writeNext("id,number".split(","));

            List<Garage> garages = garageService.readAll();

            String garageStateString;

            for (Garage garage : garages) {
                garageStateString = garage.getId() + "," + garage.getNumber();
                writer.writeNext(garageStateString.split(","));
            }

            printMessageAboutSuccessfulSaving("гаражей");

        } catch (IOException e) {
            printErrorAboutNotSaving("гаражи");
        }
    }

    private void saveMasters(File file) {
        try (CSVWriter writer = new CSVWriter(new FileWriter(file), ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER, '/', ICSVWriter.DEFAULT_LINE_END))
        {
            if (file.exists()) {
                file.delete();
            }

            writer.writeNext("id,name,status,garage".split(","));

            List<Master> masters = masterService.readAll();

            String masterStateString;

            for (Master master : masters) {
                masterStateString = master.getId() + ","
                        + master.getName() + ","
                        + master.getStatus() + ","
                        + master.getGarage().getId();
                writer.writeNext(masterStateString.split(","));
            }

            printMessageAboutSuccessfulSaving("мастеров");
        } catch (IOException e) {
            printErrorAboutNotSaving("мастеров");
        }
    }

    private void savePlaces(File file) {
        try (CSVWriter writer = new CSVWriter(new FileWriter(file), ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER, '/', ICSVWriter.DEFAULT_LINE_END))
        {
            if (file.exists()) {
                file.delete();
            }

            writer.writeNext("id,number,status,garage".split(","));

            List<Place> places = placeService.readAll();

            String placeStateString;

            for (Place place : places) {
                placeStateString = place.getId() + "," + place.getNumber() + "," + place.getStatus() + "," + place.getGarage().getId();
                writer.writeNext(placeStateString.split(","));
            }

            printMessageAboutSuccessfulSaving("мест");

        } catch (IOException e) {
            printErrorAboutNotSaving("места");
        }
    }

    private void saveOrders(File file) {
        try (CSVWriter writer = new CSVWriter(new FileWriter(file), ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER, '/', ICSVWriter.DEFAULT_LINE_END))
        {
            if (file.exists()) {
                file.delete();
            }

            writer.writeNext("id,number,dateOfReceiving,dateOfExpiration,dateOfStart,dateOfEnd,price,status,masters,place,garage".split(","));

            List<Order> orders = orderService.readAll();

            String orderStateString;

            for (Order order : orders) {
                orderStateString = order.getId() + ","
                        + order.getNumber() + ","
                        + order.getReceivingDate() + ","
                        + order.getExpirationDate() + ","
                        + order.getStartDate() + ","
                        + order.getEndDate() + "," + order.getPrice() + ","
                        + order.getStatus() + ","
                        + getMastersIdString(order) + ","
                        + order.getPlace().getId() + ","
                        + order.getGarage().getId();
                writer.writeNext(orderStateString.split(","));
            }

            printMessageAboutSuccessfulSaving("заказов");

        } catch (IOException e) {
            printErrorAboutNotSaving("заказы");
        }
    }

    private String getMastersIdString(Order order) {
        StringBuilder mastersIdString = new StringBuilder();

        for (Master master : order.getMasters()) {
            mastersIdString.append(master.getId()).append(";");
        }
        return mastersIdString.toString();
    }
}
