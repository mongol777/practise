package com.senla.autoservice.view.action.master;

import com.senla.autoservice.exceptions.EntitySortException;
import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutSortingMasters;

public class SortMasterAction implements Action {
    private final Scanner scanner;
    private final MasterService masterService;

    public SortMasterAction() {
        this.scanner = ScannerFactory.getScanner();
        this.masterService = ServiceFactory.getMasterService();
    }

    @Override
    public void execute() {
        printMessageAboutSortingMasters();
        int typeOfSort;

        try {
            typeOfSort = scanner.nextInt();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }
        printLine();

        try {
            masterService.readAll(typeOfSort).forEach(System.out::println);
        } catch (EntitySortException e) {
            System.err.println(e.getMessage());
        }
        printLine();
    }
}
