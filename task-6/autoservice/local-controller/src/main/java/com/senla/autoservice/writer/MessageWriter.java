package com.senla.autoservice.writer;

import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.status.EmploymentStatus;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.Menu;

public final class MessageWriter {
    private static final String LINE = "------------------------------";
    private static final String TRY_AGAIN = ", попробуйте снова...";

    public static void printTitle(Menu currentMenu) {
        System.out.println("\n" + currentMenu.getTitle());
        System.out.println("----------------------");
        System.out.println("Для перехода на главное меню: введите root");
        System.out.println("Для выхода из приложения: введите exit");
        System.out.println("----------------------");
    }

    public static void printErrorAboutIncorrectMenuNumber() {
        System.err.println("Неверный пункт меню, попробуйте снова...");
    }


    // Для всех сущностей
    public static void printMessageAboutCreating(String entityName) {
        switch (entityName) {
            case "Гараж":
            case "Мастер":
            case "Заказ":
                System.out.println(entityName + " создан!");
                break;
            case "Место":
                System.out.println(entityName + " создано!");
                break;
        }
    }

    public static void printMessageAboutDeleting(String entityName) {
        switch (entityName) {
            case "Гараж":
            case "Мастер":
                System.out.println(entityName + " удалён!");
                break;
            case "Место":
                System.out.println(entityName + " удалено!");
                break;
            case "Заказ":
                System.out.println(entityName + " успешно переведён в другой статус и сохранён в базе!");
                break;
        }
    }

    public static void printMessageAboutUpdating(String entityName) {
        switch (entityName) {
            case "Мастер":
            case "Заказ":
                System.out.println(entityName + " успешно обновлён!");
                break;
            case "Место":
                System.out.println(entityName + " успешно обновлено!");
                break;
        }
    }

    public static void printMessageAboutChoosingGarageForUpdate(GarageService garageService, String entityName) {
        System.out.println("Введите номер гаража, в который будем переводить " + entityName + " (0, если оставляем старый)");
        System.out.println(LINE);
        System.out.println("Все номера гаражей: ");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getNumber()));
        System.out.println(LINE);
    }

    public static void printMessageAboutChoosingGarageForCreate(GarageService garageService, String entityName) {
        switch (entityName) {
            case "мастер":
            case "заказ":
                System.out.println("Введите номер гаража, к которому будет добавлен " + entityName);
                break;
            case "место":
                System.out.println("Введите номер гаража, к которому будет добавлено " + entityName);
                break;
        }
        System.out.println(LINE);
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getNumber()));
        System.out.println(LINE);
    }

    public static void printErrorAboutIncorrectData() {
        System.err.println("Введены некорректные данные" + TRY_AGAIN);
        System.out.println(LINE);
    }

    public static void printCustomExceptionMessage(Exception e) {
        System.err.println(e.getMessage());
    }

    public static void printLine() {
        System.out.println(LINE);
    }




    // Гаражи
    public static void printErrorAboutNotFoundingGarage() {
        System.err.println("Гараж не найден" + TRY_AGAIN);
        System.out.println(LINE);
    }

    public static void printMessageAboutChoosingGarageNumberForCreate() {
        System.out.println("Введите номер для гаража: ");
    }

    public static void printMessageAboutChoosingGarageNumberForDelete() {
        System.out.println("Введите номер гаража, который хотите удалить: ");
    }






    // Мастера
    public static void printErrorAboutNotFoundingMaster() {
        System.err.println("Мастер не найден" + TRY_AGAIN);
        System.out.println(LINE);
    }

    public static void printMessageAboutSortingMasters() {
        System.out.println("Выберите способ сортировки: ");
        System.out.println(LINE);
        System.out.println("1 - по имени");
        System.out.println("2 - по статусу");
    }

    public static void printMessageAboutChoosingMasterName() {
        System.out.println("Введите имя для мастера: ");
    }

    public static void printMessageAboutChoosingMasterNameForDelete() {
        System.out.println("Введите имя мастера, которого хотите удалить: ");
    }

    public static void printMessageAboutChoosingMasterNameForUpdate() {
        System.out.println("Введите имя мастера, которого хотите изменить: ");
    }

    public static void printMessageAboutNewMasterName() {
        System.out.println("Введите новое имя мастера: ");
    }






    // Места
    public static void printErrorAboutNotFoundingPlace() {
        System.err.println("Место не найдено" + TRY_AGAIN);
        System.out.println(LINE);
    }

    public static void printMessageAboutChoosingPlaceNumber() {
        System.out.println("Введите номер для места: ");
    }

    public static void printMessageAboutChoosingPlaceNumberForDelete() {
        System.out.println("Введите номер места, которое хотите удалить: ");
    }

    public static void printMessageAboutChoosingPlaceNumberForUpdate() {
        System.out.println("Введите номер места, которое хотите изменить: ");
    }






    // Заказы
    public static void printErrorAboutNotFoundingOrder() {
        System.err.println("Заказ не найден" + TRY_AGAIN);
        System.out.println(LINE);
    }

    public static void printErrorAboutIncorrectOrderStatus() {
        System.err.println("Неверный статус заказа" + TRY_AGAIN);
        System.out.println(LINE);
    }

    public static void printMessageAboutChoosingOneOfAllOrderStatus() {
        System.out.println("Введите новый статус заказа");
        System.out.println(LINE);
        System.out.println("В - выполненный, О - отменённый, У - удалённый");
        System.out.println(LINE);
    }

    public static void printMessageAboutSortingOrders() {
        System.out.println("Выберите способ сортировки: ");
        System.out.println(LINE);
        System.out.println("1 - по дате получения");
        System.out.println("2 - по дате получения (текущие заказы)");
        System.out.println("3 - по дате примерного окончания");
        System.out.println("4 - по дате начала выполнения");
        System.out.println("5 - по дате окончания");
        System.out.println("6 - по дате получения (с временным рамками)");
        System.out.println("7 - по дате окончания (с временным рамками)");
    }

    public static void printMessageAboutNewOrderPrice() {
        System.out.println("Введите новую цену заказа: ");
    }

    public static void printMessageAboutChoosingOrderNumberForUpdate() {
        System.out.println("Введите номер заказа, который хотите изменить: ");
    }

    public static void printMessageAboutUpdatingReceivingDate() {
        System.out.println("Введите новую дату получения заказа: ");
    }

    public static void printMessageAboutUpdatingExpirationDate() {
        System.out.println("Введите новую дату примерного окончания заказа: ");
    }

    public static void printMessageAboutUpdatingStartDate() {
        System.out.println("Введите новую дату начала выполнения заказа: ");
    }

    public static void printMessageAboutUpdatingEndDate() {
        System.out.println("Введите новую дату окончания выполнения заказа: ");
    }

    public static void printMessageAboutChoosingOrderNumberForDelete() {
        System.out.println("Введите номер заказа, которого хотите удалить: ");
    }

    public static void printMessageAboutChoosingOrderNumberForAddMasterToOrder() {
        System.out.println("Введите номер заказа, к которому хотите добавить мастера: ");
    }

    public static void printMessageAboutChoosingOrderNumberForAddPlaceToOrder() {
        System.out.println("Введите номер заказа, к которому хотите добавить место: ");
    }

    public static void printMessageAboutChoosingOrderNumberForDeleteMasterFromOrder() {
        System.out.println("Введите номер заказа, из которого хотите исключить мастера: ");
    }

    public static void printMessageAboutChoosingOrderNumberForDeletePlaceFromOrder() {
        System.out.println("Введите номер заказа, из которого хотите исключить место: ");
    }

    public static void printMessageAboutChoosingDates() {
        System.out.println("Ввод дат производиться в следующем порядке: год, затем номер месяца, затем день");
    }

    public static void printMessageAboutChoosingOrderNumberForCreate() {
        System.out.println("Введите номер для заказа: ");
    }

    public static void printMessageAboutChoosingOrderPriceForCreate() {
        System.out.println("Введите цену для заказа: ");
    }

    public static void printMessageAboutCreatingReceivingDate() {
        System.out.println("Введите дату получения заказа: ");
    }

    public static void printMessageAboutCreatingExpirationDate() {
        System.out.println("Введите дату примерного окончания заказа: ");
    }

    public static void printMessageAboutCreatingStartDate() {
        System.out.println("Введите дату начала выполнения заказа: ");
    }

    public static void printMessageAboutCreatingEndDate() {
        System.out.println("Введите дату окончания выполнения заказа: ");
    }

    public static void printMessageAboutChoosingStartDateForSort() {
        System.out.println("Введите дату, с которой начинается поиск заказа: ");
    }

    public static void printMessageAboutChoosingEndDateForSort() {
        System.out.println("Введите дату, которой заканчивается поиск заказа: ");
    }

    public static void printMessageAboutChoosingMasterForAddToOrder(MasterService masterService) {
        System.out.println("Введите имя мастера, которого будем добавлять в заказ: ");
        printLine();
        System.out.println("Все мастера: ");
        masterService.readAll().stream().filter(master -> master.getStatus() == EmploymentStatus.FREE).forEach(System.out::println);
        printLine();
    }

    public static void printMessageAboutChoosingPlaceForAddToOrder(PlaceService placeService) {
        System.out.println("Введите номер места, которое будем добавлять в заказ");
        printLine();
        System.out.println("Все места: ");
        placeService.readAll().stream().filter(place -> place.getStatus() == EmploymentStatus.FREE).forEach(System.out::println);
        printLine();
    }

    public static void printMessageAboutChoosingMasterForDeleteFromOrder(Order order) {
        System.out.println("Введите имя мастера, которого будете исключать из заказа: ");
        printLine();
        System.out.println(order.getMasters());
        printLine();
    }

    public static void printMessageAboutAddingMasterToOrder() {
        System.out.println("Мастер добавлен к заказу!");
    }

    public static void printMessageAboutAddingPlaceToOrder() {
        System.out.println("Место успешно добавлено к заказу!");
    }

    public static void printMessageAboutDeletingMasterFromOrder() {
        System.out.println("Мастер успешно удалён из заказа!");
    }

    public static void printMessageAboutDeletingPlaceFromOrder() {
        System.out.println("Место успешно удалено из заказа!");
    }






    // Сохранение и загрузка
    public static void printErrorAboutNotExistingFile() {
        System.err.println("Не найден источник данных, загрузка невозможна...");
    }

    public static void printErrorAboutNotDownloading(String entityName) {
        System.err.println("Не удалось загрузить " + entityName + " по техническим причинам");
    }

    public static void printMessageAboutSuccessfulDownloading(String entityName) {
        System.out.println(LINE);
        System.out.println("Загрузка " + entityName + " прошла успешно!");
        System.out.println(LINE);
    }

    public static void printErrorAboutNotSaving(String entityName) {
        System.err.println("Не удалось сохранить " + entityName + " по техническим причинам");
    }

    public static void printMessageAboutSuccessfulSaving(String entityName) {
        System.out.println(LINE);
        System.out.println("Сохранение " + entityName + " прошла успешно!");
        System.out.println(LINE);
    }

}
