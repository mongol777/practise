package com.senla.autoservice.view.action.order;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.order.status.OrderStatus;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectOrderStatus;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingOrder;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOneOfAllOrderStatus;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForDelete;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutDeleting;

public class DeleteOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;

    public DeleteOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
    }

    @Override
    public void execute() {
        Order deletableOrder;
        try {
            deletableOrder = chooseOrder();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (deletableOrder == null) {
            printErrorAboutNotFoundingOrder();
            return;
        }

        try {
            chooseDeletableOrderStatus(deletableOrder);
        } catch (Exception e) {
            printErrorAboutIncorrectOrderStatus();
            return;
        }

        if (orderService.save(deletableOrder)) {
            printMessageAboutDeleting("Заказ");
        }

        printLine();
    }

    private void chooseDeletableOrderStatus(Order deletableOrder) throws Exception {
        printMessageAboutChoosingOneOfAllOrderStatus();
        String newOrderStatus = scanner.next();
        switch (newOrderStatus) {
            case "В":
                deletableOrder.setStatus(OrderStatus.DONE);
                break;
            case "О":
                deletableOrder.setStatus(OrderStatus.CANCELED);
                break;
            case "У":
                deletableOrder.setStatus(OrderStatus.DELETED);
                break;
            default:
                throw new Exception();
        }
    }

    private Order chooseOrder() {
        printMessageAboutChoosingOrderNumberForDelete();
        Order deletableOrder = orderService.readByNumber(scanner.nextInt());
        printLine();
        return deletableOrder;
    }
}
