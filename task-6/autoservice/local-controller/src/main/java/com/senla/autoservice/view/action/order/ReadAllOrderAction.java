package com.senla.autoservice.view.action.order;

import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import static com.senla.autoservice.writer.MessageWriter.printLine;

public class ReadAllOrderAction implements Action {
    private final OrderService orderService;

    public ReadAllOrderAction() {
        this.orderService = ServiceFactory.getOrderService();
    }

    @Override
    public void execute() {
        orderService.readAll().forEach(System.out::println);
        printLine();
    }
}
