package com.senla.autoservice.view.action.master;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingGarage;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageForCreate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingMasterName;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreating;

public class CreateMasterAction implements Action {
    private final Scanner scanner;
    private final MasterService masterService;
    private final GarageService garageService;

    public CreateMasterAction() {
        this.scanner = ScannerFactory.getScanner();
        this.masterService = ServiceFactory.getMasterService();
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        String name = createName();

        Garage garage;
        try {
            garage = chooseGarage();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (garage == null) {
            printErrorAboutNotFoundingGarage();
            return;
        }

        printLine();

        if (masterService.save(new Master(name, garage))) {
            printMessageAboutCreating("Мастер");
        }

        printLine();
    }

    private String createName() {
        printMessageAboutChoosingMasterName();
        return scanner.next();
    }

    private Garage chooseGarage() {
        printMessageAboutChoosingGarageForCreate(garageService, "мастер");
        return garageService.readByNumber(scanner.nextInt());
    }
}
