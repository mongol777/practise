package com.senla.autoservice.view.action.order;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.time.LocalDate;
import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingOrder;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingDates;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageForUpdate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForUpdate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutNewOrderPrice;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdating;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdatingEndDate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdatingExpirationDate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdatingReceivingDate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdatingStartDate;

public class UpdateOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final GarageService garageService;

    public UpdateOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        Order oldOrder;
        try {
            oldOrder = findOrderForUpdate();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (oldOrder == null) {
            printErrorAboutNotFoundingOrder();
            return;
        }

        printMessageAboutChoosingDates();
        printLine();

        try {
            oldOrder.setReceivingDate(createReceivingDate());
            oldOrder.setExpirationDate(createExpirationDate());
            oldOrder.setStartDate(createStartDate());
            oldOrder.setEndDate(createEndDate());
            oldOrder.setPrice(createNewPrice());
            oldOrder.setGarage(chooseGarage(oldOrder));
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (orderService.save(oldOrder)) {
            printMessageAboutUpdating("Заказ");
        }

        printLine();
    }

    private double createNewPrice() {
        printMessageAboutNewOrderPrice();
        return Double.parseDouble(scanner.next());
    }

    private Garage chooseGarage(Order oldOrder) {
        printMessageAboutChoosingGarageForUpdate(garageService, "заказ");

        int newGarageNumber = scanner.nextInt();

        if (newGarageNumber == 0) {
            return oldOrder.getGarage();
        }

        return garageService.readByNumber(newGarageNumber);
    }

    private Order findOrderForUpdate() {
        printMessageAboutChoosingOrderNumberForUpdate();
        int updatableOrderNumber = scanner.nextInt();
        Order oldOrder = orderService.readByNumber(updatableOrderNumber);
        printLine();
        return oldOrder;
    }

    private LocalDate createReceivingDate() {
        printMessageAboutUpdatingReceivingDate();
        return createDate();
    }

    private LocalDate createExpirationDate() {
        printMessageAboutUpdatingExpirationDate();
        return createDate();
    }

    private LocalDate createStartDate() {
        printMessageAboutUpdatingStartDate();
        return createDate();
    }

    private LocalDate createEndDate() {
        printMessageAboutUpdatingEndDate();
        return createDate();
    }

    private LocalDate createDate() {
        int yearOfReceiving = scanner.nextInt();
        int monthOfReceiving = scanner.nextInt();
        int dayOfReceiving = scanner.nextInt();
        printLine();
        return LocalDate.of(yearOfReceiving, monthOfReceiving, dayOfReceiving);
    }
}
