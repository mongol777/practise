package com.senla.autoservice.view.action.master;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageForUpdate;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingMaster;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingMasterNameForUpdate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutNewMasterName;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdating;

public class UpdateMasterAction implements Action {
    private final Scanner scanner;
    private final MasterService masterService;
    private final GarageService garageService;
    private final OrderService orderService;

    public UpdateMasterAction() {
        this.scanner = ScannerFactory.getScanner();
        this.masterService = ServiceFactory.getMasterService();
        this.garageService = ServiceFactory.getGarageService();
        this.orderService = ServiceFactory.getOrderService();
    }

    @Override
    public void execute() {
        Master master = findMasterForUpdate();

        if (master == null) {
            printErrorAboutNotFoundingMaster();
            return;
        }

        Order orderWithUpdatableMaster = orderService
                .readAll()
                .stream()
                .filter(order -> order.getMasters().contains(master))
                .findAny().orElse(null);

        if (orderWithUpdatableMaster != null) {
            orderWithUpdatableMaster.deleteMaster(master);
            updateMaster(master);
            orderWithUpdatableMaster.addMaster(master);

        } else {
            updateMaster(master);
        }
    }

    private void updateMaster(Master master) {
        String newMasterName = createNewName(master);
        printLine();

        Garage newMasterGarage;
        try {
            newMasterGarage = chooseGarage(master);
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        master.setName(newMasterName);
        master.setGarage(newMasterGarage);

        if (masterService.save(master)) {
            printMessageAboutUpdating("Мастер");
        }
        printLine();
    }

    private String createNewName(Master master) {
        printMessageAboutNewMasterName();
        String newMasterName = scanner.next();
        if (newMasterName.equals("")) {
            newMasterName = master.getName();
        }
        return newMasterName;
    }

    private Garage chooseGarage(Master master) {
        printMessageAboutChoosingGarageForUpdate(garageService, "мастера");
        int newGarageNumber = scanner.nextInt();
        if (newGarageNumber == 0) {
            return master.getGarage();
        }
        return garageService.readByNumber(newGarageNumber);
    }

    private Master findMasterForUpdate() {
        printMessageAboutChoosingMasterNameForUpdate();
        Master oldMaster = masterService.readByName(scanner.next());
        printLine();
        return oldMaster;

    }
}
