package com.senla.autoservice.view;

import com.senla.autoservice.view.action.Action;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MenuItem {
    private String title;
    private Action action;
    private Menu nextMenu;

    public void doAction() {
        action.execute();
    }
}
