package com.senla.autoservice.view;

import lombok.Data;

import static com.senla.autoservice.writer.MessageWriter.printTitle;

@Data
public class MenuNavigator {
    private Menu currentMenu;

    public void printMenu() {
        printTitle(currentMenu);

        for (int i = 0; i < currentMenu.getItems().size(); i++) {
            System.out.println(i + 1 + " - " + currentMenu.getItems().get(i).getTitle());
        }
    }

    public void navigate(int index) {
        MenuItem item = currentMenu.getItems().get(index);
        if (item.getAction() == null) {
            currentMenu = item.getNextMenu();
        } else {
            item.doAction();
        }
    }
}
