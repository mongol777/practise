package com.senla.autoservice.view.action.order.master;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingMaster;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingOrder;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutAddingMasterToOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingMasterForAddToOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForAddMasterToOrder;

public class AddMasterToOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final MasterService masterService;

    public AddMasterToOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
        this.masterService = ServiceFactory.getMasterService();
    }

    @Override
    public void execute() {
        Order order;
        try {
            order = findOrderForAddMaster();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (order == null) {
            printErrorAboutNotFoundingOrder();
            return;
        }

        Master master = chooseMaster();

        if (master == null) {
            printErrorAboutNotFoundingMaster();
            return;
        }

        order.addMaster(master);

        if (orderService.save(order)) {
            printMessageAboutAddingMasterToOrder();
        }

        printLine();
    }

    private Master chooseMaster() {
        printMessageAboutChoosingMasterForAddToOrder(masterService);
        return masterService.readByName(scanner.next());
    }


    private Order findOrderForAddMaster() {
        printMessageAboutChoosingOrderNumberForAddMasterToOrder();
        Order order = orderService.readByNumber(scanner.nextInt());
        printLine();
        return order;

    }
}
