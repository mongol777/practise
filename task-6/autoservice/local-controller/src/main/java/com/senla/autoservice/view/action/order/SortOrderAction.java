package com.senla.autoservice.view.action.order;

import com.senla.autoservice.exceptions.EntitySortException;
import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.time.LocalDate;
import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printCustomExceptionMessage;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingEndDateForSort;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingStartDateForSort;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutSortingOrders;

public class SortOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;

    public SortOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
    }

    @Override
    public void execute() {
        printMessageAboutSortingOrders();
        int typeOfSort;

        try {
            typeOfSort = scanner.nextInt();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        printLine();

        switch (typeOfSort) {
            case 6:
                try {
                    orderService.readAllWithBeforeAndAfterReceivingDate(createStartDate(), createEndDate()).forEach(System.out::println);
                } catch (Exception e) {
                    printErrorAboutIncorrectData();
                }
                break;
            case 7:
                try {
                    orderService.readAllWithBeforeAndAfterEndingDate(createStartDate(), createEndDate()).forEach(System.out::println);
                } catch (Exception e) {
                    printErrorAboutIncorrectData();
                }
                break;
            default:
                try {
                    orderService.readAll(typeOfSort).forEach(System.out::println);
                } catch (EntitySortException e) {
                    printCustomExceptionMessage(e);
                }
                break;
        }

        printLine();
    }

    private LocalDate createStartDate() {
        printMessageAboutChoosingStartDateForSort();
        return createDate();
    }

    private LocalDate createEndDate() {
        printMessageAboutChoosingEndDateForSort();
        return createDate();
    }

    private LocalDate createDate() {
        int yearOfReceiving = scanner.nextInt();
        int monthOfReceiving = scanner.nextInt();
        int dayOfReceiving = scanner.nextInt();
        printLine();
        return LocalDate.of(yearOfReceiving, monthOfReceiving, dayOfReceiving);
    }
}
