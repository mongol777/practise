package com.senla.autoservice.view.action.place;

import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import static com.senla.autoservice.writer.MessageWriter.printLine;

public class ReadAllPlaceAction implements Action {
    private final PlaceService placeService;

    public ReadAllPlaceAction() {
        this.placeService = ServiceFactory.getPlaceService();
    }

    @Override
    public void execute() {
        placeService.readAll().forEach(System.out::println);
        printLine();
    }
}
