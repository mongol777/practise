package com.senla.autoservice.view.action.order.place;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingOrder;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingPlace;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutAddingPlaceToOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForAddPlaceToOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingPlaceForAddToOrder;

public class AddPlaceToOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final PlaceService placeService;

    public AddPlaceToOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
        this.placeService = ServiceFactory.getPlaceService();
    }

    @Override
    public void execute() {
        Order order;
        try {
            order = findOrderForAddPlace();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (order == null) {
            printErrorAboutNotFoundingOrder();
            return;
        }

        Place place;
        try {
            place = choosePlace();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (place == null) {
            printErrorAboutNotFoundingPlace();
            return;
        }

        order.setPlace(place);
        if (orderService.save(order)) {
            printMessageAboutAddingPlaceToOrder();
        }

        printLine();
    }

    private Place choosePlace() {
        printMessageAboutChoosingPlaceForAddToOrder(placeService);
        return placeService.readByNumber(scanner.nextInt());
    }


    private Order findOrderForAddPlace() {
        printMessageAboutChoosingOrderNumberForAddPlaceToOrder();
        Order order = orderService.readByNumber(scanner.nextInt());
        printLine();
        return order;
    }
}
