package com.senla.autoservice.view.action.garage;

import com.senla.autoservice.exceptions.EntityDeleteException;
import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printCustomExceptionMessage;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingGarage;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageNumberForDelete;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutDeleting;

public class DeleteGarageAction implements Action {
    private final Scanner scanner;
    private final GarageService garageService;

    public DeleteGarageAction() {
        this.scanner = ScannerFactory.getScanner();
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        printMessageAboutChoosingGarageNumberForDelete();
        int deletableGarageNumber;

        try {
            deletableGarageNumber = Integer.parseInt(scanner.next());
        } catch (Exception ex) {
            printErrorAboutIncorrectData();
            return;
        }

        Garage deletableGarage = garageService.readByNumber(deletableGarageNumber);
        printLine();

        if (deletableGarage == null) {
            printErrorAboutNotFoundingGarage();
            return;
        }

        try {
            if (garageService.delete(deletableGarage.getId())) {
                printMessageAboutDeleting("Гараж");
            }
        } catch (EntityNotFoundException | EntityDeleteException e) {
            printCustomExceptionMessage(e);
        }
        printLine();
    }
}
