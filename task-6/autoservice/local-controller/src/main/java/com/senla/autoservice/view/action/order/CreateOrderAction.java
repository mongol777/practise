package com.senla.autoservice.view.action.order;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.time.LocalDate;
import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingGarage;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingDates;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageForCreate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForCreate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderPriceForCreate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreating;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreatingEndDate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreatingExpirationDate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreatingReceivingDate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreatingStartDate;

public class CreateOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final GarageService garageService;

    public CreateOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        Garage garage;
        try {
            garage = chooseGarage();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (garage == null) {
            printErrorAboutNotFoundingGarage();
            return;
        }

        double price;
        try {
            price = createPrice();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        int number;
        try {
            number = createNumber();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        printMessageAboutChoosingDates();
        printLine();

        LocalDate receivingDate;
        LocalDate expirationDate;
        LocalDate startDate;
        LocalDate endDate;

        try {
            receivingDate = createReceivingDate();
            expirationDate = createExpirationDate();
            startDate = createStartDate();
            endDate = createEndDate();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (orderService.save(new Order(number, receivingDate, expirationDate, startDate, endDate, price, garage))) {
            printMessageAboutCreating("Заказ");
        }

        printLine();
    }

    private int createNumber() {
        printMessageAboutChoosingOrderNumberForCreate();
        int number = scanner.nextInt();
        printLine();
        return number;
    }

    private Garage chooseGarage() {
        printMessageAboutChoosingGarageForCreate(garageService, "заказ");
        return garageService.readByNumber(scanner.nextInt());
    }

    private double createPrice() {
        printMessageAboutChoosingOrderPriceForCreate();
        double price = Double.parseDouble(scanner.next());
        printLine();
        return price;
    }

    private LocalDate createReceivingDate() {
        printMessageAboutCreatingReceivingDate();
        return createDate();
    }

    private LocalDate createExpirationDate() {
        printMessageAboutCreatingExpirationDate();
        return createDate();
    }

    private LocalDate createStartDate() {
        printMessageAboutCreatingStartDate();
        return createDate();
    }

    private LocalDate createEndDate() {
        printMessageAboutCreatingEndDate();
        return createDate();
    }

    private LocalDate createDate() {
        int yearOfEnd = scanner.nextInt();
        int monthOfEnd = scanner.nextInt();
        int dayOfEnd = scanner.nextInt();
        printLine();
        return LocalDate.of(yearOfEnd, monthOfEnd, dayOfEnd);
    }
}
