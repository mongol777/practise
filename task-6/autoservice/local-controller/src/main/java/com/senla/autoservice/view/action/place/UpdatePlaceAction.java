package com.senla.autoservice.view.action.place;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingPlace;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageForUpdate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingPlaceNumberForUpdate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutUpdating;

public class UpdatePlaceAction implements Action {

    private final Scanner scanner;
    private final PlaceService placeService;
    private final GarageService garageService;
    private final OrderService orderService;

    public UpdatePlaceAction() {
        this.scanner = ScannerFactory.getScanner();
        this.placeService = ServiceFactory.getPlaceService();
        this.garageService = ServiceFactory.getGarageService();
        this.orderService = ServiceFactory.getOrderService();
    }

    @Override
    public void execute() {
        Place oldPlace;
        try {
            oldPlace = findPlaceForUpdate();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (oldPlace == null) {
            printErrorAboutNotFoundingPlace();
            return;
        }

        Order orderWithUpdatablePlace = orderService
                .readAll()
                .stream()
                .filter(order -> order.getPlace().equals(oldPlace))
                .findAny().orElse(null);

        if (orderWithUpdatablePlace != null) {
            orderWithUpdatablePlace.setPlace(null);
            updatePlace(oldPlace);
            orderWithUpdatablePlace.setPlace(oldPlace);
        } else {
            updatePlace(oldPlace);
        }
        printLine();
    }

    private void updatePlace(Place oldPlace) {
        Garage newPlaceGarage;
        try {
            newPlaceGarage = chooseGarage(oldPlace);
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        oldPlace.setGarage(newPlaceGarage);

        if (placeService.save(oldPlace)) {
            printMessageAboutUpdating("Место");
        }

    }

    private Garage chooseGarage(Place oldPlace) {
        printMessageAboutChoosingGarageForUpdate(garageService, "место");
        int newGarageNumber = scanner.nextInt();
        if (newGarageNumber == 0) {
            return oldPlace.getGarage();
        }
        printLine();
        return garageService.readByNumber(newGarageNumber);
    }

    private Place findPlaceForUpdate() {
        printMessageAboutChoosingPlaceNumberForUpdate();
        int updatablePlaceNumber = scanner.nextInt();
        Place oldPlace = placeService.readByNumber(updatablePlaceNumber);
        printLine();
        return oldPlace;
    }
}
