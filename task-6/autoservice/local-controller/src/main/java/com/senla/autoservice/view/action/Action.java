package com.senla.autoservice.view.action;

public interface Action {
    void execute();
}
