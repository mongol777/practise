package com.senla.autoservice.view.action.garage;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingGarageNumberForCreate;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutCreating;

public class CreateGarageAction implements Action {
    private final Scanner scanner;
    private final GarageService garageService;

    public CreateGarageAction() {
        this.scanner = ScannerFactory.getScanner();
        this.garageService = ServiceFactory.getGarageService();
    }

    @Override
    public void execute() {
        printMessageAboutChoosingGarageNumberForCreate();
        int garageNumber;
        try {
            garageNumber = scanner.nextInt();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (garageService.save(new Garage(garageNumber))) {
            printMessageAboutCreating("Гараж");
        }

        printLine();
    }
}
