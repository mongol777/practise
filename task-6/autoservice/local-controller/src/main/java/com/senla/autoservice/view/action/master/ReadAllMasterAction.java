package com.senla.autoservice.view.action.master;

import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

import static com.senla.autoservice.writer.MessageWriter.printLine;

public class ReadAllMasterAction implements Action {
    private final MasterService masterService;

    public ReadAllMasterAction() {
        this.masterService = ServiceFactory.getMasterService();
    }

    @Override
    public void execute() {
        masterService.readAll().forEach(System.out::println);
        printLine();
    }
}
