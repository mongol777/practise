package com.senla.autoservice.view.action.order.master;

import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingMaster;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingOrder;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingMasterForDeleteFromOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingOrderNumberForDeleteMasterFromOrder;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutDeletingMasterFromOrder;

public class DeleteMasterFromOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final MasterService masterService;

    public DeleteMasterFromOrderAction() {
        this.scanner = ScannerFactory.getScanner();
        this.orderService = ServiceFactory.getOrderService();
        this.masterService = ServiceFactory.getMasterService();
    }

    @Override
    public void execute() {
        Order order;
        try {
            order = findOrderForDeleteMaster();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        if (order == null) {
            printErrorAboutNotFoundingOrder();
            return;
        }

        Master master = chooseMaster(order);

        if (master == null) {
            printErrorAboutNotFoundingMaster();
            return;
        }

        order.deleteMaster(master);

        if (orderService.save(order)) {
            printMessageAboutDeletingMasterFromOrder();
        }

        printLine();
    }

    private Master chooseMaster(Order order) {
        printMessageAboutChoosingMasterForDeleteFromOrder(order);
        String masterName = scanner.next();
        printLine();
        return masterService.readByName(masterName);
    }


    private Order findOrderForDeleteMaster() {
        printMessageAboutChoosingOrderNumberForDeleteMasterFromOrder();
        Order order = orderService.readByNumber(scanner.nextInt());
        printLine();
        return order;
    }
}
