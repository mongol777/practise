package com.senla.autoservice.view.action.place;

import com.senla.autoservice.exceptions.EntityDeleteException;
import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.factory.scanner.ScannerFactory;
import com.senla.autoservice.factory.service.ServiceFactory;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

import static com.senla.autoservice.writer.MessageWriter.printErrorAboutIncorrectData;
import static com.senla.autoservice.writer.MessageWriter.printErrorAboutNotFoundingPlace;
import static com.senla.autoservice.writer.MessageWriter.printLine;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutChoosingPlaceNumberForDelete;
import static com.senla.autoservice.writer.MessageWriter.printMessageAboutDeleting;

public class DeletePlaceAction implements Action {
    private final Scanner scanner;
    private final PlaceService placeService;

    public DeletePlaceAction() {
        this.scanner = ScannerFactory.getScanner();
        this.placeService = ServiceFactory.getPlaceService();
    }

    @Override
    public void execute() {
        int deletablePlaceNumber;
        try {
            deletablePlaceNumber = createNumber();
        } catch (Exception e) {
            printErrorAboutIncorrectData();
            return;
        }

        printLine();

        Place place = placeService.readByNumber(deletablePlaceNumber);

        if (place == null) {
            printErrorAboutNotFoundingPlace();
            return;
        }

        try {
            if (placeService.delete(place.getId())) {
                printMessageAboutDeleting("Место");
            }
        } catch (EntityNotFoundException | EntityDeleteException e) {
            System.err.println(e.getMessage());
        }

        printLine();
    }

    private int createNumber() {
        printMessageAboutChoosingPlaceNumberForDelete();
        return scanner.nextInt();
    }
}
