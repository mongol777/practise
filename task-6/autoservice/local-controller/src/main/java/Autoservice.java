import com.senla.autoservice.view.MenuBuilder;
import com.senla.autoservice.view.MenuController;
import com.senla.autoservice.view.MenuNavigator;

public class Autoservice {
    public static void main(String[] args) {
        new MenuController(new MenuNavigator(), new MenuBuilder()).run();
    }
}
