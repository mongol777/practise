package com.senla.autoservice.factory.scanner;

import java.util.Scanner;

public final class ScannerFactory {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static Scanner getScanner() {
        return SCANNER;
    }
}
