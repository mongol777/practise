package com.senla.autoservice.factory.service;

import com.senla.autoservice.factory.repository.RepositoryFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.garage.CommonGarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.master.CommonMasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.order.CommonOrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.service.place.CommonPlaceService;

public final class ServiceFactory {
    private static final MasterService MASTER_SERVICE = new CommonMasterService(RepositoryFactory.getMasterRepository());
    private static final OrderService ORDER_SERVICE = new CommonOrderService(RepositoryFactory.getOrderRepository());
    private static final PlaceService PLACE_SERVICE = new CommonPlaceService(RepositoryFactory.getPlaceRepository());
    private static final GarageService GARAGE_SERVICE = new CommonGarageService(RepositoryFactory.getGarageRepository(), getMasterService(), getPlaceService(), getOrderService());

    public static GarageService getGarageService() {
        return GARAGE_SERVICE;
    }

    public static MasterService getMasterService() {
        return MASTER_SERVICE;
    }

    public static OrderService getOrderService() {
        return ORDER_SERVICE;
    }

    public static PlaceService getPlaceService() {
        return PLACE_SERVICE;
    }
}
