package com.senla.autoservice.factory.repository;

import com.senla.autoservice.repository.garage.CommonGarageRepository;
import com.senla.autoservice.repository.garage.GarageRepository;
import com.senla.autoservice.repository.master.CommonMasterRepository;
import com.senla.autoservice.repository.master.MasterRepository;
import com.senla.autoservice.repository.order.CommonOrderRepository;
import com.senla.autoservice.repository.order.OrderRepository;
import com.senla.autoservice.repository.place.CommonPlaceRepository;
import com.senla.autoservice.repository.place.PlaceRepository;

public final class RepositoryFactory {
    private static final GarageRepository GARAGE_REPOSITORY = new CommonGarageRepository();
    private static final MasterRepository MASTER_REPOSITORY = new CommonMasterRepository();
    private static final OrderRepository ORDER_REPOSITORY = new CommonOrderRepository();
    private static final PlaceRepository PLACE_REPOSITORY = new CommonPlaceRepository();

    public static GarageRepository getGarageRepository() {
        return GARAGE_REPOSITORY;
    }

    public static MasterRepository getMasterRepository() {
        return MASTER_REPOSITORY;
    }

    public static OrderRepository getOrderRepository() {
        return ORDER_REPOSITORY;
    }

    public static PlaceRepository getPlaceRepository() {
        return PLACE_REPOSITORY;
    }

}
