package com.senla.autoservice.repository.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.repository.Repository;

import java.util.List;

public interface PlaceRepository extends Repository<Place> {
    List<Place> readByGarage(Garage garage);
    Place readByNumber(int number);
}
