package com.senla.autoservice.service.order;

import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.exceptions.EntitySortException;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.order.status.OrderStatus;
import com.senla.autoservice.models.status.EmploymentStatus;
import com.senla.autoservice.repository.order.OrderRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommonOrderService implements OrderService {

    private final OrderRepository orderRepository;

    public CommonOrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public boolean save(Order newOrder) {
        return orderRepository.save(newOrder);
    }

    @Override
    public Order readById(long id) {
        return orderRepository.readById(id);
    }

    @Override
    public Order readOrderByMaster(Master master) {
        return orderRepository
                .readAll()
                .stream()
                .filter(order -> order.getMasters().contains(master))
                .findAny().orElse(null);
    }

    @Override
    public List<Order> readAll() {
        return orderRepository.readAll();
    }

    @Override
    public Order readByNumber(int number) {
        return orderRepository.readByNumber(number);
    }

    @Override
    public List<Order> readByGarage(Garage garage) {
        return orderRepository.readByGarage(garage);
    }

    @Override
    public boolean delete(long id) throws EntityNotFoundException {
        Order deletableOrder = readById(id);
        if (deletableOrder == null) {
            throw new EntityNotFoundException("Удаляемый заказ не найден, попробуйте снова...");
        }

        deletableOrder.getMasters().forEach(master -> master.setStatus(EmploymentStatus.FREE));

        if (deletableOrder.getPlace() != null) {
            deletableOrder.getPlace().setStatus(EmploymentStatus.FREE);
        }

        return orderRepository.delete(id);
    }

    @Override
    public List<Order> readAll(int typeOfSort) throws EntitySortException {
        List<Order> sortedOrders = new ArrayList<>();

        switch (typeOfSort) {
            case 1:
                sortedOrders.addAll(orderRepository.readAll()
                        .stream()
                        .sorted(Comparator.comparing(Order::getReceivingDate))
                        .collect(Collectors.toList()));
                break;
            case 2:
                sortedOrders.addAll(orderRepository.readAll()
                        .stream()
                        .filter(order -> order.getStatus() == OrderStatus.IN_PROGRESS)
                        .sorted(Comparator.comparing(Order::getReceivingDate))
                        .collect(Collectors.toList()));
                break;
            case 3:
                sortedOrders.addAll(orderRepository.readAll()
                        .stream()
                        .sorted(Comparator.comparing(Order::getExpirationDate))
                        .collect(Collectors.toList()));
                break;
            case 4:
                sortedOrders.addAll(orderRepository.readAll()
                        .stream()
                        .sorted(Comparator.comparing(Order::getStartDate))
                        .collect(Collectors.toList()));
                break;
            case 5:
                sortedOrders.addAll(orderRepository.readAll()
                        .stream()
                        .sorted(Comparator.comparing(Order::getEndDate))
                        .collect(Collectors.toList()));
                break;
            default:
                throw new EntitySortException("Неверный тип сортировки, попробуйте снова...");
        }
        return sortedOrders;
    }

    @Override
    public List<Order> readAllWithBeforeAndAfterReceivingDate(LocalDate afterDate, LocalDate beforeDate) {
        return orderRepository.readAll()
                .stream()
                .filter(order -> order.getReceivingDate().isAfter(afterDate) && order.getReceivingDate().isBefore(beforeDate))
                .sorted(Comparator.comparing(Order::getReceivingDate))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> readAllWithBeforeAndAfterEndingDate(LocalDate beforeDate, LocalDate afterDate) {
        return orderRepository.readAll()
                .stream()
                .filter(order -> order.getEndDate().isAfter(afterDate) && order.getEndDate().isBefore(beforeDate))
                .sorted(Comparator.comparing(Order::getEndDate))
                .collect(Collectors.toList());
    }

}
