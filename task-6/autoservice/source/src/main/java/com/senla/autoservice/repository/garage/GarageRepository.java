package com.senla.autoservice.repository.garage;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.repository.Repository;

public interface GarageRepository extends Repository<Garage> {
    Garage readByNumber(int number);
}
