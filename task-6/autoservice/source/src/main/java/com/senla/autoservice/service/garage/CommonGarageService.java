package com.senla.autoservice.service.garage;


import com.senla.autoservice.exceptions.EntityDeleteException;
import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.repository.garage.GarageRepository;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;

import java.util.List;

public class CommonGarageService implements GarageService {

    private final GarageRepository garageRepository;
    private final MasterService masterService;
    private final PlaceService placeService;
    private final OrderService orderService;

    public CommonGarageService(GarageRepository garageRepository,
                               MasterService masterService,
                               PlaceService placeService,
                               OrderService orderService) {
        this.garageRepository = garageRepository;
        this.masterService = masterService;
        this.placeService = placeService;
        this.orderService = orderService;
    }

    @Override
    public boolean save(Garage garage) {
        return garageRepository.save(garage);
    }

    @Override
    public Garage readById(long id) {
        return garageRepository.readById(id);
    }

    @Override
    public Garage readByNumber(int number) {
        return garageRepository.readByNumber(number);
    }

    @Override
    public List<Garage> readAll() {
        return garageRepository.readAll();
    }

    @Override
    public boolean delete(long id) throws EntityNotFoundException, EntityDeleteException {
        Garage garage = readById(id);
        if (garage == null) {
            throw new EntityNotFoundException("Удаляемый гараж не найден, попробуйте снова...");
        }

        if (!masterService.readByGarage(garage).isEmpty()) {
            throw new EntityDeleteException("В удаляемом гараже еще есть мастера...");
        }

        if (!placeService.readByGarage(garage).isEmpty()) {
            throw new EntityDeleteException("В удаляемом гараже еще есть места...");
        }

        if (!orderService.readByGarage(garage).isEmpty()) {
            throw new EntityDeleteException("В удаляемом гараже еще есть заказы...");
        }

        return garageRepository.delete(id);
    }
}

