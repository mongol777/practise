package com.senla.autoservice.models.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.status.EmploymentStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@AllArgsConstructor
@Getter
@Setter
public class Place {
    private long id;
    private int number;
    private EmploymentStatus status;
    private Garage garage;

    public Place(int number, Garage garage) {
        this.number = number;
        this.status = EmploymentStatus.FREE;
        this.garage = garage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return id == place.id && number == place.number && status == place.status && garage.equals(place.garage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, status, garage);
    }

    @Override
    public String toString() {
        return "[Место №" + number +
                ", статус: " + status +
                ", находится в гараже №" + garage.getNumber() + "]";
    }
}
