package com.senla.autoservice.exceptions;

public class EntityDeleteException extends Exception {
    public EntityDeleteException(String message) {
        super(message);
    }
}
