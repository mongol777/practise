package com.senla.autoservice.models.order.status;

public enum OrderStatus {
    CANCELED,
    DELETED,
    DONE,
    IN_PROGRESS

}
