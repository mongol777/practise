package com.senla.autoservice.service;

import com.senla.autoservice.exceptions.EntityDeleteException;
import com.senla.autoservice.exceptions.EntityNotFoundException;

import java.util.List;

public interface Service<T> {
    boolean save(T entity);
    T readById(long id);
    List<T> readAll();
    boolean delete(long id) throws EntityNotFoundException, EntityDeleteException;
}
