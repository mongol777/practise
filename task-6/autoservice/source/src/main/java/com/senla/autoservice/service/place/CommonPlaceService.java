package com.senla.autoservice.service.place;

import com.senla.autoservice.exceptions.EntityDeleteException;
import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.models.status.EmploymentStatus;
import com.senla.autoservice.repository.place.PlaceRepository;

import java.util.List;
import java.util.stream.Collectors;

public class CommonPlaceService implements PlaceService {

    private final PlaceRepository placeRepository;

    public CommonPlaceService(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Override
    public boolean save(Place newPlace) {
        return placeRepository.save(newPlace);
    }

    @Override
    public Place readById(long id) {
        return placeRepository.readById(id);
    }

    @Override
    public List<Place> readByGarage(Garage garage) {
        return placeRepository.readByGarage(garage);
    }

    @Override
    public List<Place> readAll() {
        return placeRepository.readAll();
    }

    @Override
    public Place readByNumber(int number) {
        return placeRepository.readByNumber(number);
    }

    @Override
    public boolean delete(long id) throws EntityNotFoundException, EntityDeleteException {
        Place place = readById(id);
        if (place == null) {
            throw new EntityNotFoundException("Удаляемое место не найдено, попробуйте снова...");
        }

        if (place.getStatus() == EmploymentStatus.BUSY) {
            throw new EntityDeleteException("На этом месте ещё производятся работы...");
        }

        return placeRepository.delete(id);
    }

    @Override
    public List<Place> getPlacesWithFreeStatus() {
        return readAll().stream().filter(place -> place.getStatus() == EmploymentStatus.FREE).collect(Collectors.toList());
    }
}
