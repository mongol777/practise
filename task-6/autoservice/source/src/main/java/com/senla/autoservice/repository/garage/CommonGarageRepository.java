package com.senla.autoservice.repository.garage;

import com.senla.autoservice.models.garage.Garage;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public class CommonGarageRepository implements GarageRepository {
    private static final Map<Long, Garage> GARAGE_MAP = new HashMap<>();
    private static long garageID = 1;

    @Override
    public boolean save(Garage newGarage) {
        if (newGarage.getId() != 0) {
            garageID = newGarage.getId();
        }
        newGarage.setId(garageID);
        GARAGE_MAP.put(garageID, newGarage);

        do {
            garageID++;
        } while (GARAGE_MAP.containsKey(garageID));

        return GARAGE_MAP.containsValue(newGarage);
    }

    @Override
    public Garage readById(long id) {
        return GARAGE_MAP.get(id);

    }

    @Override
    public Garage readByNumber(int number) {
        return readAll().stream().filter(garage -> garage.getNumber() == number).findAny().orElse(null);
    }

    @Override
    public List<Garage> readAll() {
        return new ArrayList<>(GARAGE_MAP.values());
    }

    @Override
    public boolean delete(long id) {
        GARAGE_MAP.remove(id);
        return !GARAGE_MAP.containsKey(id);

    }
}
