package com.senla.autoservice.repository.order;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
public class CommonOrderRepository implements OrderRepository {
    private static final Map<Long, Order> ORDER_MAP = new HashMap<>();
    private static long orderID = 1;

    @Override
    public boolean save(Order newOrder) {
        if (newOrder.getId() != 0) {
            orderID = newOrder.getId();
        }
        newOrder.setId(orderID);
        ORDER_MAP.put(orderID, newOrder);
        do {
            orderID++;
        } while (ORDER_MAP.containsKey(orderID));

        return ORDER_MAP.containsValue(newOrder);
    }

    @Override
    public Order readById(long id) {
        if (ORDER_MAP.containsKey(id)) {
            return ORDER_MAP.get(id);
        }
        return null;
    }

    @Override
    public Order readByNumber(int number) {
        return ORDER_MAP.values().stream().filter(order -> order.getNumber() == number).findAny().orElse(null);
    }

    @Override
    public List<Order> readByGarage(Garage garage) {
        return ORDER_MAP.values().stream().filter(order -> order.getGarage().equals(garage)).collect(Collectors.toList());
    }

    @Override
    public List<Order> readAll() {
        return new ArrayList<>(ORDER_MAP.values());
    }

    @Override
    public boolean delete(long id) {
        ORDER_MAP.remove(id);
        return !ORDER_MAP.containsKey(id);
    }
}
