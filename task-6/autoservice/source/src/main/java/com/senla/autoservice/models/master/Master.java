package com.senla.autoservice.models.master;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.status.EmploymentStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
public class Master {
    private long id;
    private String name;
    private EmploymentStatus status;
    private Garage garage;

    public Master(String name, Garage garage) {
        this.status = EmploymentStatus.FREE;
        this.name = name;
        this.garage = garage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Master master = (Master) o;
        return id == master.id && name.equals(master.name) && status == master.status && garage.equals(master.garage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, status, garage);
    }

    @Override
    public String toString() {
        return "[Мастер " + name +
                ", статус: " + status +
                ", работает в гараже №" + garage.getNumber() +
                "]";
    }
}
