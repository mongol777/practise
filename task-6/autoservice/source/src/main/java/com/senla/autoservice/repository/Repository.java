package com.senla.autoservice.repository;

import java.util.List;

public interface Repository<T> {
    boolean save(T entity);
    T readById(long id);
    List<T> readAll();
    boolean delete(long id);
}
