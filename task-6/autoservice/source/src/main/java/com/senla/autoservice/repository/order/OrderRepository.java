package com.senla.autoservice.repository.order;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.repository.Repository;

import java.util.List;

public interface OrderRepository extends Repository<Order> {
    List<Order> readByGarage(Garage garage);
    Order readByNumber(int number);
}
