package com.senla.autoservice.repository.master;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.repository.Repository;

import java.util.List;

public interface MasterRepository extends Repository<Master> {
    List<Master> readByGarage(Garage garage);
    Master readByName(String name);
}
