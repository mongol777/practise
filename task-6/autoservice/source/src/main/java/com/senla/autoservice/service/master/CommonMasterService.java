package com.senla.autoservice.service.master;

import com.senla.autoservice.exceptions.EntityNotFoundException;
import com.senla.autoservice.exceptions.EntitySortException;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.repository.master.MasterRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommonMasterService implements MasterService {

    private final MasterRepository masterRepository;

    public CommonMasterService(MasterRepository masterRepository) {
        this.masterRepository = masterRepository;
    }

    @Override
    public boolean save(Master newMaster) {
        return masterRepository.save(newMaster);
    }

    @Override
    public Master readById(long id) {
        return masterRepository.readById(id);
    }

    @Override
    public List<Master> readByOrder(Order order) {
        return new ArrayList<>(order.getMasters());
    }

    @Override
    public Master readByName(String name) {
        return masterRepository.readByName(name);
    }

    @Override
    public List<Master> readByGarage(Garage garage) {
        return masterRepository.readByGarage(garage);
    }

    @Override
    public List<Master> readAll() {
        return masterRepository.readAll();
    }

    @Override
    public boolean delete(long id) throws EntityNotFoundException {
        Master master = readById(id);
        if (master == null) {
            throw new EntityNotFoundException("Удаляемый мастер не найден, попробуйте снова...");
        }
        return masterRepository.delete(id);
    }

    @Override
    public List<Master> readAll(int typeOfSort) throws EntitySortException {
        List<Master> sortedMasters = new ArrayList<>();
        switch (typeOfSort) {
            case 1:
                sortedMasters.addAll(masterRepository.readAll()
                        .stream()
                        .sorted(Comparator.comparing(Master::getName))
                        .collect(Collectors.toList()));
                break;
            case 2:
                sortedMasters.addAll(masterRepository.readAll()
                        .stream()
                        .sorted(Comparator.comparing(Master::getStatus))
                        .collect(Collectors.toList()));
                break;
            default:
                throw new EntitySortException("Неверный пункт сортировки, попробуйте снова...");
        }
        return sortedMasters;
    }
}
