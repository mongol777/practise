package com.senla.autoservice.service.master;

import com.senla.autoservice.exceptions.EntitySortException;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.Service;

import java.util.List;

public interface MasterService extends Service<Master> {
    Master readByName(String name);
    List<Master> readByGarage(Garage garage);
    List<Master> readByOrder(Order order);
    List<Master> readAll(int typeOfSort) throws EntitySortException;
}
