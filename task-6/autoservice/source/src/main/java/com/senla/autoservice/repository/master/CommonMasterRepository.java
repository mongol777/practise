package com.senla.autoservice.repository.master;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
public class CommonMasterRepository implements MasterRepository {
    private static final Map<Long, Master> MASTER_MAP = new HashMap<>();
    private static long masterID = 1;

    @Override
    public boolean save(Master newMaster) {
        if (newMaster.getId() != 0) {
            masterID = newMaster.getId();
        }
        newMaster.setId(masterID);
        MASTER_MAP.put(masterID, newMaster);

        do {
            masterID++;
        } while (MASTER_MAP.containsKey(masterID));

        return MASTER_MAP.containsValue(newMaster);
    }

    @Override
    public Master readById(long id) {
        if (MASTER_MAP.containsKey(id)) {
            return MASTER_MAP.get(id);
        }
        return null;
    }

    @Override
    public Master readByName(String name) {
        return MASTER_MAP.values().stream().filter(master -> master.getName().equals(name)).findAny().orElse(null);
    }

    @Override
    public List<Master> readByGarage(Garage garage) {
        return MASTER_MAP.values().stream().filter(master -> master.getGarage().equals(garage)).collect(Collectors.toList());
    }

    @Override
    public List<Master> readAll() {
        return new ArrayList<>(MASTER_MAP.values());
    }

    @Override
    public boolean delete(long id) {
        MASTER_MAP.remove(id);
        return !MASTER_MAP.containsKey(id);
    }
}
