package com.senla.autoservice.service.order;

import com.senla.autoservice.exceptions.EntitySortException;
import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.Service;

import java.time.LocalDate;
import java.util.List;

public interface OrderService extends Service<Order> {
    Order readByNumber(int number);
    Order readOrderByMaster(Master master);
    List<Order> readByGarage(Garage garage);
    List<Order> readAll(int typeOfSort) throws EntitySortException;
    List<Order> readAllWithBeforeAndAfterReceivingDate(LocalDate beforeDate, LocalDate afterDate);
    List<Order> readAllWithBeforeAndAfterEndingDate(LocalDate beforeDate, LocalDate afterDate);

}
