package com.senla.autoservice.models.order;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.status.OrderStatus;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.models.status.EmploymentStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
public class Order {
    private long id;
    private int number;
    private LocalDate receivingDate;
    private LocalDate expirationDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private double price;
    private OrderStatus status;
    private Set<Master> masters;
    private Place place;
    private Garage garage;

    public Order(int number, LocalDate receivingDate, LocalDate expirationDate, LocalDate startDate, LocalDate endDate, double price, Garage garage) {
        this.masters = new HashSet<>();
        this.number = number;
        this.status = OrderStatus.IN_PROGRESS;
        this.receivingDate = receivingDate;
        this.expirationDate = expirationDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.garage = garage;
    }

    public void addMaster(Master master) {
        List<Master> masters = new ArrayList<>(this.masters);
        master.setStatus(EmploymentStatus.BUSY);
        masters.add(master);
        this.masters = new HashSet<>(masters);
    }

    public void deleteMaster(Master master) {
        List<Master> masters = new ArrayList<>(this.masters);
        master.setStatus(EmploymentStatus.FREE);
        masters.remove(master);
        this.masters = new HashSet<>(masters);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && number == order.number && Double.compare(order.price, price) == 0 && receivingDate.equals(order.receivingDate) && expirationDate.equals(order.expirationDate) && startDate.equals(order.startDate) && endDate.equals(order.endDate) && status == order.status && Objects.equals(place, order.place) && Objects.equals(garage, order.garage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, receivingDate, expirationDate, startDate, endDate, price, status, place, garage);
    }

    @Override
    public String toString() {
        String result = "[Заказ №" + number +
                ", дата получения: " + receivingDate +
                ", дата примерного окончания: " + expirationDate +
                ", дата начала выполнения: " + startDate +
                ", дата окончания: " + endDate +
                ", цена: " + price +
                ", статус: " + status;
        if (place == null) {
            result = result + ", нет места";
        } else {
            result = result + ", место №" + place.getNumber();
        }

        if (masters.isEmpty()) {
            result = result + ", нет мастеров";
        } else {
            result = result + ", мастера: " + masters;
        }

        result = result + ", гараж №" + garage.getNumber() + "]";
        return result;
    }
}
