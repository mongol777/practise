package com.senla.autoservice.models.status;

public enum EmploymentStatus {
    BUSY,
    FREE
}
