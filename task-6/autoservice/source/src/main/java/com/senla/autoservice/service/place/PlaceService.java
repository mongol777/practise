package com.senla.autoservice.service.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.Service;

import java.util.List;

public interface PlaceService extends Service<Place> {
    List<Place> readByGarage(Garage garage);
    Place readByNumber(int number);
    List<Place> getPlacesWithFreeStatus();
}
