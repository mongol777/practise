package com.senla.autoservice.repository.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.Place;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
public class CommonPlaceRepository implements PlaceRepository {
    private static final Map<Long, Place> PLACE_MAP = new HashMap<>();
    private static long placeID = 1;

    @Override
    public boolean save(Place newPlace) {
        if (newPlace.getId() != 0) {
            placeID = newPlace.getId();
        }
        newPlace.setId(placeID);
        PLACE_MAP.put(placeID, newPlace);

        do {
            placeID++;
        } while (PLACE_MAP.containsKey(placeID));

        return PLACE_MAP.containsValue(newPlace);
    }

    @Override
    public Place readById(long id) {
        if (PLACE_MAP.containsKey(id)) {
            return PLACE_MAP.get(id);
        }
        return null;
    }

    @Override
    public Place readByNumber(int number) {
        return PLACE_MAP.values().stream().filter(place -> place.getNumber() == number).findAny().orElse(null);
    }

    @Override
    public List<Place> readByGarage(Garage garage) {
        return PLACE_MAP.values().stream().filter(place -> place.getGarage().equals(garage)).collect(Collectors.toList());
    }

    @Override
    public List<Place> readAll() {
        return new ArrayList<>(PLACE_MAP.values());
    }

    @Override
    public boolean delete(long id) {
        PLACE_MAP.remove(id);
        return !PLACE_MAP.containsKey(id);
    }
}
