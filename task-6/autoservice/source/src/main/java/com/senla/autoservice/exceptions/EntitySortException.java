package com.senla.autoservice.exceptions;

public class EntitySortException extends Exception {
    public EntitySortException(String message) {
        super(message);
    }
}
