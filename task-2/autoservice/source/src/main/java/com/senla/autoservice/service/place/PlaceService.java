package com.senla.autoservice.service.place;

import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.Service;

import java.util.List;

public interface PlaceService extends Service<Place> {
    List<Place> placesWithFreeStatus();
}
