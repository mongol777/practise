package com.senla.autoservice.service.garage;


import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.repository.garage.GarageRepository;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;

import java.util.List;

public class CommonGarageService implements GarageService {

    private final GarageRepository garageRepository;
    private final MasterService masterService;
    private final PlaceService placeService;
    private final OrderService orderService;

    public CommonGarageService(GarageRepository garageRepository,
                               MasterService masterService,
                               PlaceService placeService,
                               OrderService orderService) {
        this.garageRepository = garageRepository;
        this.masterService = masterService;
        this.placeService = placeService;
        this.orderService = orderService;
    }

    @Override
    public boolean save(Garage garage) {
        return garageRepository.save(garage);
    }

    @Override
    public Garage read(long id) {
        return garageRepository.read(id);
    }

    @Override
    public List<Garage> readAll() {
        return garageRepository.readAll();
    }

    @Override
    public boolean update(long id, Garage newGarage) {
        Garage garage = read(id);
        if (garage != null) {
            return garageRepository.update(id, newGarage);
        }
        return false;
    }

    @Override
    public boolean delete(long id) {
        Garage garage = read(id);
        if (garage != null) {
            masterService.readAll()
                    .stream()
                    .filter(master -> master.getGarage().equals(garage))
                    .findAny()
                    .ifPresent(master -> masterService.delete(master.getId()));
            placeService.readAll()
                    .stream()
                    .filter(place -> place.getGarage().equals(garage))
                    .findAny()
                    .ifPresent(place -> placeService.delete(place.getId()));
            orderService.readAll()
                    .stream()
                    .filter(order -> order.getGarage().equals(garage))
                    .findAny()
                    .ifPresent(order -> orderService.delete(order.getId()));
            return garageRepository.delete(id);
        }
        return false;
    }
}
