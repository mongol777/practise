package com.senla.autoservice.models.order;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.master.status.MasterStatus;
import com.senla.autoservice.models.order.status.OrderStatus;
import com.senla.autoservice.models.place.Place;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class Order {
    private long id;
    private LocalDate dateOfReceiving;
    private LocalDate dateOfExpiration;
    private LocalDate dateOfStart;
    private LocalDate dateOfEnd;
    private double price;
    private OrderStatus status;
    private Set<Master> masters = new HashSet<>();
    private Place place;
    private Garage garage;

    public Order(LocalDate dateOfReceiving, LocalDate dateOfExpiration, LocalDate dateOfStart, LocalDate dateOfEnd, double price, Garage garage) {
        this.status = OrderStatus.IN_PROGRESS;
        this.dateOfReceiving = dateOfReceiving;
        this.dateOfExpiration = dateOfExpiration;
        this.dateOfStart = dateOfStart;
        this.dateOfEnd = dateOfEnd;
        this.price = price;
        this.garage = garage;
    }

    public void addMaster(Master master) {
        List<Master> masters = new ArrayList<>(this.masters);
        master.setStatus(MasterStatus.BUSY);
        masters.add(master);
        this.masters = new HashSet<>(masters);
    }

    public void deleteMaster(Master master) {
        List<Master> masters = new ArrayList<>(this.masters);
        master.setStatus(MasterStatus.FREE);
        masters.remove(master);
        this.masters = new HashSet<>(masters);
    }

    @Override
    public String toString() {
        String result = "[Заказ №" + id +
                ", дата получения: " + dateOfReceiving +
                ", дата примерного окончания: " + dateOfExpiration +
                ", дата начала выполнения: " + dateOfStart +
                ", дата окончания: " + dateOfEnd +
                ", цена: " + price +
                ", статус: " + status;
        if (place == null) {
            result = result + ", нет места";
        } else {
            result = result + ", место №" + place.getId();
        }

        if (masters.isEmpty()) {
            result = result + ", нет мастеров";
        } else {
            result = result + ", мастера: " + masters;
        }

        result = result + ", гараж №" + garage.getId() + "]";
        return result;
    }
}
