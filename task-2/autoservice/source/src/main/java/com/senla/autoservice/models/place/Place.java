package com.senla.autoservice.models.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.status.PlaceStatus;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class Place {
    private long id;
    private PlaceStatus status;
    private Garage garage;

    public Place(Garage garage) {
        this.status = PlaceStatus.FREE;
        this.garage = garage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return id == place.id && status == place.status && garage.equals(place.garage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, garage);
    }

    @Override
    public String toString() {
        return "[Место №" + id +
                ", статус: " + status +
                ", находится в гараже №" + garage.getId() + "]";
    }
}
