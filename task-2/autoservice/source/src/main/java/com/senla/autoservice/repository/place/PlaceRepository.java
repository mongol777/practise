package com.senla.autoservice.repository.place;

import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.repository.Repository;

public interface PlaceRepository extends Repository<Place> {
}
