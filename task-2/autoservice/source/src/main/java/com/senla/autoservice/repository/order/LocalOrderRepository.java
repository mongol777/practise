package com.senla.autoservice.repository.order;

import com.senla.autoservice.models.order.Order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalOrderRepository implements OrderRepository {
    private static final Map<Long, Order> ORDER_MAP = new HashMap<>();
    private static long orderID = 1;

    public LocalOrderRepository() {
    }

    @Override
    public boolean save(Order newOrder) {
        newOrder.setId(orderID);
        ORDER_MAP.put(orderID, newOrder);
        orderID++;
        return ORDER_MAP.containsValue(newOrder);
    }

    @Override
    public Order read(long id) {
        if (ORDER_MAP.containsKey(id)) {
            return ORDER_MAP.get(id);
        }
        return null;
    }

    @Override
    public List<Order> readAll() {
        return new ArrayList<>(ORDER_MAP.values());
    }

    @Override
    public boolean update(long id, Order updatedOrder) {
        ORDER_MAP.replace(id, updatedOrder);
        return ORDER_MAP.containsValue(updatedOrder);

    }

    @Override
    public boolean delete(long id) {
        ORDER_MAP.remove(id);
        return !ORDER_MAP.containsKey(id);
    }
}
