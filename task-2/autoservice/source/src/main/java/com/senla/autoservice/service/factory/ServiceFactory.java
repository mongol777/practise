package com.senla.autoservice.service.factory;

import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;

public interface ServiceFactory {
    GarageService getGarageService();
    MasterService getMasterService();
    OrderService getOrderService();
    PlaceService getPlaceService();
}
