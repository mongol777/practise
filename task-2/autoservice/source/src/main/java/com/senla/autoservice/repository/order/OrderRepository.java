package com.senla.autoservice.repository.order;

import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.repository.Repository;

public interface OrderRepository extends Repository<Order> {
}
