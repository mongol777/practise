package com.senla.autoservice.service.factory;

import com.senla.autoservice.repository.factory.LocalRepositoryFactory;
import com.senla.autoservice.repository.factory.RepositoryFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.garage.CommonGarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.master.CommonMasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.order.CommonOrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.service.place.CommonPlaceService;

public class CommonServiceFactory implements ServiceFactory {
    private final GarageService garageService;
    private final MasterService masterService;
    private final OrderService orderService;
    private final PlaceService placeService;

    public CommonServiceFactory() {
        RepositoryFactory repositoryFactory = new LocalRepositoryFactory();
        this.orderService = new CommonOrderService(repositoryFactory.getOrderRepository());

        this.masterService = new CommonMasterService(repositoryFactory.getMasterRepository(),
                this.orderService);

        this.placeService = new CommonPlaceService(repositoryFactory.getPlaceRepository(),
                this.orderService);

        this.garageService = new CommonGarageService(repositoryFactory.getGarageRepository(),
                this.masterService, this.placeService, this.orderService);
    }


    @Override
    public GarageService getGarageService() {
        return garageService;
    }

    @Override
    public MasterService getMasterService() {
        return masterService;
    }

    @Override
    public OrderService getOrderService() {
        return orderService;
    }

    @Override
    public PlaceService getPlaceService() {
        return placeService;
    }
}
