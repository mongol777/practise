package com.senla.autoservice.repository.factory;

import com.senla.autoservice.repository.garage.GarageRepository;
import com.senla.autoservice.repository.master.MasterRepository;
import com.senla.autoservice.repository.order.OrderRepository;
import com.senla.autoservice.repository.place.PlaceRepository;

public interface RepositoryFactory {
    GarageRepository getGarageRepository();
    MasterRepository getMasterRepository();
    OrderRepository getOrderRepository();
    PlaceRepository getPlaceRepository();
}
