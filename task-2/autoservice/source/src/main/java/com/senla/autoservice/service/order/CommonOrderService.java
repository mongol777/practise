package com.senla.autoservice.service.order;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.master.status.MasterStatus;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.place.status.PlaceStatus;
import com.senla.autoservice.repository.order.OrderRepository;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommonOrderService implements OrderService {

    private final OrderRepository orderRepository;

    public CommonOrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public boolean save(Order newOrder) {
        return orderRepository.save(newOrder);
    }

    @Override
    public Order read(long id) {
        return orderRepository.read(id);
    }

    @Override
    public Order readOrderByMaster(Master master) {
        return orderRepository
                .readAll()
                .stream()
                .filter(order -> order.getMasters().contains(master))
                .findAny().orElse(null);
    }

    @Override
    public List<Order> readAll() {
        return orderRepository.readAll();
    }

    @Override
    public boolean update(long id, Order updatedOrder) {
        return orderRepository.update(id, updatedOrder);
    }

    @Override
    public boolean delete(long id) {
        Order deletableOrder = read(id);
        if (deletableOrder != null) {
            deletableOrder.getMasters().forEach(master -> master.setStatus(MasterStatus.FREE));

            if (deletableOrder.getPlace() != null) {
                deletableOrder.getPlace().setStatus(PlaceStatus.FREE);
            }

            return orderRepository.delete(id);
        }
        return false;
    }

    @Override
    public List<Order> sortOrdersByReceivingDate() {
        return orderRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Order::getDateOfReceiving))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> sortOrdersByReceivingDateInProgress() {
        return orderRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Order::getDateOfReceiving))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> sortOrdersByExpirationDate() {
        return orderRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Order::getDateOfExpiration))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> sortOrdersByStartDate() {
        return orderRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Order::getDateOfStart))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> sortOrdersByEndDate() {
        return orderRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Order::getDateOfEnd))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> sortOrdersWithBeforeAndAfterReceivingDate(LocalDate afterDate, LocalDate beforeDate) {
        return orderRepository.readAll()
                .stream()
                .filter(order -> order.getDateOfReceiving().isAfter(afterDate) && order.getDateOfReceiving().isBefore(beforeDate))
                .sorted(Comparator.comparing(Order::getDateOfReceiving))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> sortOrdersWithBeforeAndAfterEndingDate(LocalDate beforeDate, LocalDate afterDate) {
        return orderRepository.readAll()
                .stream()
                .filter(order -> order.getDateOfReceiving().isAfter(afterDate) && order.getDateOfReceiving().isBefore(beforeDate))
                .sorted(Comparator.comparing(Order::getDateOfEnd))
                .collect(Collectors.toList());
    }

}
