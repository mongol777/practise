package com.senla.autoservice.service;

import java.util.List;

public interface Service<T> {
    boolean save(T entity);
    T read(long id);
    List<T> readAll();
    boolean update(long id, T updatableEntity);
    boolean delete(long id);
}
