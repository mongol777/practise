package com.senla.autoservice.repository.place;

import com.senla.autoservice.models.place.Place;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalPlaceRepository implements PlaceRepository {
    private static final Map<Long, Place> PLACE_MAP = new HashMap<>();
    private static long placeID = 1;

    public LocalPlaceRepository() {}

    @Override
    public boolean save(Place newPlace) {
        newPlace.setId(placeID);
        PLACE_MAP.put(placeID, newPlace);
        placeID++;
        return PLACE_MAP.containsValue(newPlace);
    }

    @Override
    public Place read(long id) {
        if (PLACE_MAP.containsKey(id)) {
            return PLACE_MAP.get(id);
        }
        return null;
    }

    @Override
    public List<Place> readAll() {
        return new ArrayList<>(PLACE_MAP.values());
    }

    @Override
    public boolean update(long id, Place updatePlace) {
        PLACE_MAP.replace(id, updatePlace);
        return PLACE_MAP.containsValue(updatePlace);
    }

    @Override
    public boolean delete(long id) {
        PLACE_MAP.remove(id);
        return !PLACE_MAP.containsKey(id);
    }
}
