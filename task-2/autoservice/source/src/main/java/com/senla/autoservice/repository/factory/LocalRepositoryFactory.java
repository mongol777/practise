package com.senla.autoservice.repository.factory;

import com.senla.autoservice.repository.garage.GarageRepository;
import com.senla.autoservice.repository.garage.LocalGarageRepository;
import com.senla.autoservice.repository.master.MasterRepository;
import com.senla.autoservice.repository.master.LocalMasterRepository;
import com.senla.autoservice.repository.order.OrderRepository;
import com.senla.autoservice.repository.order.LocalOrderRepository;
import com.senla.autoservice.repository.place.PlaceRepository;
import com.senla.autoservice.repository.place.LocalPlaceRepository;

public class LocalRepositoryFactory implements RepositoryFactory {

    private final GarageRepository garageRepository;
    private final MasterRepository masterRepository;
    private final OrderRepository orderRepository;
    private final PlaceRepository placeRepository;

    public LocalRepositoryFactory() {
        this.orderRepository = new LocalOrderRepository();
        this.masterRepository = new LocalMasterRepository();
        this.placeRepository = new LocalPlaceRepository();
        this.garageRepository = new LocalGarageRepository();
    }

    @Override
    public GarageRepository getGarageRepository() {
        return garageRepository;
    }

    @Override
    public MasterRepository getMasterRepository() {
        return masterRepository;
    }

    @Override
    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    @Override
    public PlaceRepository getPlaceRepository() {
        return placeRepository;
    }
}
