package com.senla.autoservice.service.place;

import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.repository.place.PlaceRepository;
import com.senla.autoservice.service.order.OrderService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommonPlaceService implements PlaceService {

    private final PlaceRepository placeRepository;
    private final OrderService orderService;

    public CommonPlaceService(PlaceRepository placeRepository, OrderService orderService) {
        this.placeRepository = placeRepository;
        this.orderService = orderService;
    }

    @Override
    public boolean save(Place newPlace) {
        return placeRepository.save(newPlace);
    }

    @Override
    public Place read(long id) {
        return placeRepository.read(id);
    }

    @Override
    public List<Place> readAll() {
        return placeRepository.readAll();
    }

    @Override
    public boolean update(long id, Place updatedPlace) {
        return placeRepository.update(id, updatedPlace);
    }

    @Override
    public boolean delete(long id) {
        Place place = read(id);
        if (place != null) {
            orderService
                    .readAll()
                    .stream()
                    .filter(order -> order.getPlace().equals(place))
                    .findAny().ifPresent(order -> order.setPlace(null));
            return placeRepository.delete(id);
        } else {
            System.out.println("Место не найдено, попробуйте снова...");
            return false;
        }
    }

    @Override
    public List<Place> placesWithFreeStatus() {
        return readAll().stream().sorted(Comparator.comparing(Place::getStatus)).collect(Collectors.toList());
    }
}
