package com.senla.autoservice.repository.master;

import com.senla.autoservice.models.master.Master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalMasterRepository implements MasterRepository {
    private static final Map<Long, Master> MASTER_MAP = new HashMap<>();
    private static long masterID = 1;

    public LocalMasterRepository() {}

    @Override
    public boolean save(Master newMaster) {
        newMaster.setId(masterID);
        MASTER_MAP.put(masterID, newMaster);
        masterID++;
        return MASTER_MAP.containsValue(newMaster);
    }

    @Override
    public Master read(long id) {
        if (MASTER_MAP.containsKey(id)) {
            return MASTER_MAP.get(id);
        }
        return null;
    }

    @Override
    public List<Master> readAll() {
        return new ArrayList<>(MASTER_MAP.values());
    }

    @Override
    public boolean update(long id, Master updatedMaster) {
        MASTER_MAP.replace(id, updatedMaster);
        return MASTER_MAP.containsValue(updatedMaster);
    }

    @Override
    public boolean delete(long id) {
        MASTER_MAP.remove(id);
        return !MASTER_MAP.containsKey(id);
    }
}
