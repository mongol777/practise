package com.senla.autoservice.service.garage;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.service.Service;

public interface GarageService extends Service<Garage> {
}
