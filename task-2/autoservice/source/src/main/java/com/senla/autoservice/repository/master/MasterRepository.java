package com.senla.autoservice.repository.master;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.repository.Repository;

public interface MasterRepository extends Repository<Master> {
}
