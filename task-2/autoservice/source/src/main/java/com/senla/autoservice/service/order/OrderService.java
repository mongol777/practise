package com.senla.autoservice.service.order;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.Service;

import java.time.LocalDate;
import java.util.List;

public interface OrderService extends Service<Order> {
    Order readOrderByMaster(Master master);

    List<Order> sortOrdersByReceivingDate();
    List<Order> sortOrdersByReceivingDateInProgress();
    List<Order> sortOrdersByExpirationDate();
    List<Order> sortOrdersByStartDate();
    List<Order> sortOrdersByEndDate();
    List<Order> sortOrdersWithBeforeAndAfterReceivingDate(LocalDate beforeDate, LocalDate afterDate);
    List<Order> sortOrdersWithBeforeAndAfterEndingDate(LocalDate beforeDate, LocalDate afterDate);

}
