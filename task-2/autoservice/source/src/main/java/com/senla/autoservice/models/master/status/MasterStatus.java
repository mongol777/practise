package com.senla.autoservice.models.master.status;

public enum MasterStatus {
    BUSY,
    FREE
}
