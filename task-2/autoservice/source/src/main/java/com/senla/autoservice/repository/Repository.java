package com.senla.autoservice.repository;

import java.util.List;

public interface Repository<T> {
    boolean save(T entity);
    T read(long id);
    List<T> readAll();
    boolean update(long id, T updatableEntity);
    boolean delete(long id);
}
