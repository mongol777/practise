package com.senla.autoservice.service.master;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.Service;

import java.util.List;

public interface MasterService extends Service<Master> {
    List<Master> readMastersByOrder(Order order);
    List<Master> sortMastersByName();
    List<Master> sortMastersByStatus();
}
