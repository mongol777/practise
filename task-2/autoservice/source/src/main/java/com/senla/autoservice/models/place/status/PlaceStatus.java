package com.senla.autoservice.models.place.status;

public enum PlaceStatus {
    BUSY,
    FREE
}
