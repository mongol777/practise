package com.senla.autoservice.service.master;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.repository.master.MasterRepository;
import com.senla.autoservice.service.order.OrderService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommonMasterService implements MasterService {

    private final MasterRepository masterRepository;
    private final OrderService orderService;

    public CommonMasterService(MasterRepository masterRepository, OrderService orderService) {
        this.masterRepository = masterRepository;
        this.orderService = orderService;
    }

    @Override
    public boolean save(Master newMaster) {
        return masterRepository.save(newMaster);
    }

    @Override
    public Master read(long id) {
        return masterRepository.read(id);
    }

    @Override
    public List<Master> readMastersByOrder(Order order) {
        return new ArrayList<>(order.getMasters());
    }

    @Override
    public List<Master> readAll() {
        return masterRepository.readAll();
    }

    @Override
    public boolean update(long id, Master updatedMaster) {
        return masterRepository.update(id, updatedMaster);
    }

    @Override
    public boolean delete(long id) {
        Master master = read(id);
        if (master != null) {
            orderService
                    .readAll()
                    .stream()
                    .filter(order -> order.getMasters().contains(master))
                    .findAny().ifPresent(order -> order.deleteMaster(master));
            return masterRepository.delete(id);
        } else {
            System.out.println("Мастер не найден, попробуйте снова...");
            return false;
        }
    }

    @Override
    public List<Master> sortMastersByName() {
        return masterRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Master::getName))
                .collect(Collectors.toList());
    }

    @Override
    public List<Master> sortMastersByStatus() {
        return masterRepository.readAll()
                .stream()
                .sorted(Comparator.comparing(Master::getStatus))
                .collect(Collectors.toList());
    }
}
