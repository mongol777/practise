package com.senla.autoservice.repository.garage;

import com.senla.autoservice.models.garage.Garage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalGarageRepository implements GarageRepository {
    private static final Map<Long, Garage> GARAGE_MAP = new HashMap<>();
    private static long garageID = 1;

    public LocalGarageRepository() {}

    @Override
    public boolean save(Garage garage) {
        garage.setId(garageID);
        GARAGE_MAP.put(garageID, garage);
        garageID++;
        return GARAGE_MAP.containsValue(garage);
    }

    @Override
    public Garage read(long id) {
        if (GARAGE_MAP.containsKey(id)) {
            return GARAGE_MAP.get(id);
        }
        return null;
    }

    @Override
    public List<Garage> readAll() {
        return new ArrayList<>(GARAGE_MAP.values());
    }

    @Override
    public boolean update(long id, Garage newGarage) {
        GARAGE_MAP.replace(id, newGarage);
        return GARAGE_MAP.containsValue(newGarage);
    }

    @Override
    public boolean delete(long id) {
        GARAGE_MAP.remove(id);
        return !GARAGE_MAP.containsKey(id);

    }
}
