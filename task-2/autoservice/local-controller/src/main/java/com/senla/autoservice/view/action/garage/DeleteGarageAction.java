package com.senla.autoservice.view.action.garage;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class DeleteGarageAction implements Action {
    private final Scanner scanner;
    private final GarageService garageService;

    public DeleteGarageAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Введите номер гаража, который хотите удалить");
        int deletableGarageId = Integer.parseInt(scanner.next());
        Garage deletableGarage = garageService.read(deletableGarageId);
        System.out.println("------------------------------");

        if (deletableGarage != null) {
            if (garageService.delete(deletableGarage.getId())) {
                System.out.println("Гараж успешно удалён!");
            } else {
                System.out.println("Не удалось удалить гараж по техническим причинам");
            }
        } else {
            System.out.println("Гараж не найден, попробуйте снова...");
        }
        System.out.println("------------------------------");
    }
}
