package com.senla.autoservice.view.action.order.place;

import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class DeletePlaceFromOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final PlaceService placeService;

    public DeletePlaceFromOrderAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.orderService = serviceFactory.getOrderService();
        this.placeService = serviceFactory.getPlaceService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Order order = findOrderForDeletePlace();
        if (order != null) {
            Place place = choosePlace(order);

            if (place != null) {
                order.setPlace(null);
                if (orderService.update(order.getId(), order)) {
                    System.out.println("Место успешно удалено из заказа!");
                } else {
                    System.out.println("Не удалось обновить заказ по техническим причинам");
                }
            } else {
                System.out.println("Место не найдено. Повторите попытку...");
            }
        } else {
            System.out.println("Заказ не найден, повторите попытку...");
        }
        System.out.println("------------------------------");
    }

    private Place choosePlace(Order order) {
        System.out.println("Введите номер места, которое хотите исключить из заказа");
        System.out.println("------------------------------");
        System.out.println(order.getMasters());
        System.out.println("------------------------------");
        int masterId = scanner.nextInt();
        System.out.println("------------------------------");

        return placeService.read(masterId);
    }


    private Order findOrderForDeletePlace() {
        System.out.println("Введите номер заказа, из которого хотите исключить мастера");
        int updatableOrderId = Integer.parseInt(scanner.next());
        Order order = orderService.read(updatableOrderId);
        System.out.println("------------------------------");

        return order;
    }
}
