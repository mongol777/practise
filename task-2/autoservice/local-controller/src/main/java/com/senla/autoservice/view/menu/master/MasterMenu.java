package com.senla.autoservice.view.menu.master;

import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.List;

public class MasterMenu implements Menu {
    private List<MenuItem> menuItems;

    public MasterMenu() {}

    @Override
    public String getTitle() {
        return "Меню мастеров";
    }

    @Override
    public List<MenuItem> getItems() {
        return menuItems;
    }

    @Override
    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
