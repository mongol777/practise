package com.senla.autoservice.view.action.garage;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.view.action.Action;

public class ReadAllGarageAction implements Action {
    private final GarageService garageService;

    public ReadAllGarageAction(ServiceFactory serviceFactory) {
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Все гаражи: ");
        garageService.readAll().forEach(garage -> System.out.println(garage + "\n"));
        System.out.println("------------------------------");
    }
}
