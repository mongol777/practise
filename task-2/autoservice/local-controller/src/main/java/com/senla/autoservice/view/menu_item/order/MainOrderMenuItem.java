package com.senla.autoservice.view.menu_item.order;

import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

public class MainOrderMenuItem implements MenuItem {
    private Menu nextMenu;

    public MainOrderMenuItem() {
    }

    public String getTitle() {
        return "Меню заказов\n";
    }

    @Override
    public Action getAction() {
        throw new RuntimeException("Тут нельзя получить action");
    }

    @Override
    public void doAction() {
        throw new RuntimeException("Тут нельзя делать action");
    }

    @Override
    public void setNextMenu(Menu nextMenu) {
        this.nextMenu = nextMenu;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
