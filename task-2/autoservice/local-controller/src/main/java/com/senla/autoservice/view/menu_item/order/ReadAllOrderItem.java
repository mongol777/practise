package com.senla.autoservice.view.menu_item.order;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.order.ReadAllOrderAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

public class ReadAllOrderItem implements MenuItem {
    private final Action action;

    public ReadAllOrderItem(ServiceFactory serviceFactory) {
        this.action = new ReadAllOrderAction(serviceFactory);
    }

    public String getTitle() {
        return "Все заказы";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
