package com.senla.autoservice.view.menu_item.order.master;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.order.master.AddMasterToOrderAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.Scanner;

public class AddMasterToOrderItem implements MenuItem {
    private final Action action;
    private final Scanner scanner;

    public AddMasterToOrderItem(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.action = new AddMasterToOrderAction(scanner, serviceFactory);
    }

    public Scanner getScanner() {
        return scanner;
    }

    public String getTitle() {
        return "Добавление мастера к заказу";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
