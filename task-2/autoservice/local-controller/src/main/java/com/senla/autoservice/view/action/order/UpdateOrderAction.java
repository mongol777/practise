package com.senla.autoservice.view.action.order;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.time.LocalDate;
import java.util.Scanner;

public class UpdateOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final GarageService garageService;

    public UpdateOrderAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.orderService = serviceFactory.getOrderService();
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Order oldOrder = findOrderForUpdate();
        if (oldOrder != null) {

            Garage newOrderGarage = chooseGarage(oldOrder);

            System.out.println("Ввод новых дат производиться в следующем порядке: год, затем номер месяца, затем день");
            System.out.println("------------------------------");

            LocalDate newDateOfReceiving = createReceivingDate();
            LocalDate newDateOfExpiration = createExpirationDate();
            LocalDate newDateOfStart = createStartDate();
            LocalDate newDateOfEnd = createEndDate();

            System.out.println("Введите новую цену");
            double newPrice = Double.parseDouble(scanner.next());
            System.out.println("------------------------------");

            oldOrder.setDateOfReceiving(newDateOfReceiving);
            oldOrder.setDateOfExpiration(newDateOfExpiration);
            oldOrder.setDateOfStart(newDateOfStart);
            oldOrder.setDateOfEnd(newDateOfEnd);
            oldOrder.setPrice(newPrice);
            oldOrder.setGarage(newOrderGarage);

            if (orderService.update(oldOrder.getId(), oldOrder)) {
                System.out.println("Заказ успешно обновлён!");
            } else {
                System.out.println("Не удалось обновить заказ по техническим причинам");
            }
        } else {
            System.out.println("Заказ не найден, попробуйте снова...");
        }
        System.out.println("------------------------------");
    }

    private Garage chooseGarage(Order oldOrder) {
        System.out.println("Введите номер гаража, в который будем переводить заказ (0, если оставляем старый)");
        System.out.println("------------------------------");
        System.out.println("Все номера гаражей: ");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getId()));
        System.out.println("------------------------------");

        Garage newOrderGarage;
        int newGarageId = Integer.parseInt(scanner.next());
        if (newGarageId != 0) {
            newOrderGarage = garageService.read(newGarageId);
        } else {
            newOrderGarage = oldOrder.getGarage();
        }
        System.out.println("------------------------------");
        return newOrderGarage;
    }

    private Order findOrderForUpdate() {
        System.out.println("Введите номер заказа, который хотите изменить");
        int updatableOrderId = Integer.parseInt(scanner.next());
        Order oldOrder = orderService.read(updatableOrderId);
        System.out.println("------------------------------");

        return oldOrder;
    }

    private LocalDate createReceivingDate() {
        System.out.println("Введите новую дату получения заказа ");
        int yearOfReceiving = scanner.nextInt();
        int monthOfReceiving = scanner.nextInt();
        int dayOfReceiving = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfReceiving, monthOfReceiving, dayOfReceiving);
    }

    private LocalDate createExpirationDate() {
        System.out.println("Введите новую дату примерного окончания заказа ");
        int yearOfExpiration = scanner.nextInt();
        int monthOfExpiration = scanner.nextInt();
        int dayOfExpiration = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfExpiration, monthOfExpiration, dayOfExpiration);
    }

    private LocalDate createStartDate() {
        System.out.println("Введите новую дату начала выполнения заказа ");
        int yearOfStart = scanner.nextInt();
        int monthOfStart = scanner.nextInt();
        int dayOfStart = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfStart, monthOfStart, dayOfStart);
    }

    private LocalDate createEndDate() {
        System.out.println("Введите новую дату окончания выполнения заказа ");
        int yearOfEnd = scanner.nextInt();
        int monthOfEnd = scanner.nextInt();
        int dayOfEnd = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfEnd, monthOfEnd, dayOfEnd);
    }
}
