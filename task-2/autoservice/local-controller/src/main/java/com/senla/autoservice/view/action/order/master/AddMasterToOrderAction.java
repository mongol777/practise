package com.senla.autoservice.view.action.order.master;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.master.status.MasterStatus;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class AddMasterToOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final MasterService masterService;

    public AddMasterToOrderAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.orderService = serviceFactory.getOrderService();
        this.masterService = serviceFactory.getMasterService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Order order = findOrderForAddMaster();
        if (order != null) {
            Master master = chooseMaster();

            if (master != null) {
                order.addMaster(master);
                if (orderService.update(order.getId(), order)) {
                    System.out.println("Мастер успешно добавлен к заказу!");
                } else {
                    System.out.println("Не удалось обновить заказ по техническим причинам");
                }
            } else {
                System.out.println("Мастер не найден. Повторите попытку...");
            }
        } else {
            System.out.println("Заказ не найден, повторите попытку...");
        }
        System.out.println("------------------------------");
    }

    private Master chooseMaster() {
        System.out.println("Введите номер мастера, которого будем добавлять в заказ");
        System.out.println("------------------------------");
        System.out.println("Все мастера: ");
        masterService.readAll().stream().filter(master -> master.getStatus() == MasterStatus.FREE).forEach(System.out::println);
        System.out.println("------------------------------");
        int masterId = scanner.nextInt();
        System.out.println("------------------------------");

        return masterService.read(masterId);
    }


    private Order findOrderForAddMaster() {
        System.out.println("Введите номер заказа, к которому хотите добавить мастера");
        int updatableOrderId = Integer.parseInt(scanner.next());
        Order order = orderService.read(updatableOrderId);
        System.out.println("------------------------------");

        return order;
    }
}
