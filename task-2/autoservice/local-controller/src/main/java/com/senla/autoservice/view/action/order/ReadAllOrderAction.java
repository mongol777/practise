package com.senla.autoservice.view.action.order;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

public class ReadAllOrderAction implements Action {
    private final OrderService orderService;

    public ReadAllOrderAction(ServiceFactory serviceFactory) {
        this.orderService = serviceFactory.getOrderService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Все заказы: ");
        orderService.readAll().forEach(System.out::println);
        System.out.println("------------------------------");
    }
}
