package com.senla.autoservice.view.menu_item.main;

import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.main.MainMenuAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.Scanner;

public class MainMenuItem implements MenuItem {

    private final Action action;

    private final Scanner scanner;

    public MainMenuItem(Scanner scanner) {
        this.scanner = scanner;
        this.action = new MainMenuAction(scanner);
    }

    public Scanner getScanner() {
        return scanner;
    }

    public Action getAction() {
        return action;
    }

    public String getTitle() {
        return "Главное меню";
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка. Дальше меню нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка. Дальше меню нет.");
    }
}
