package com.senla.autoservice.view.menu.garage;

import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.List;

public class GarageMenu implements Menu {
    private List<MenuItem> menuItems;

    public GarageMenu() {}

    @Override
    public String getTitle() {
        return "Меню гаражей";
    }

    @Override
    public List<MenuItem> getItems() {
        return menuItems;
    }

    @Override
    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
