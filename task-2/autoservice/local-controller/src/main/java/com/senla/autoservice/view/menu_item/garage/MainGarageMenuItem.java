package com.senla.autoservice.view.menu_item.garage;

import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

public class MainGarageMenuItem implements MenuItem {
    private Menu nextMenu;

    public MainGarageMenuItem() {
    }

    public String getTitle() {
        return "Меню гаражей\n";
    }

    @Override
    public Action getAction() {
        throw new RuntimeException("Тут нельзя получить action");
    }

    @Override
    public void doAction() {
        throw new RuntimeException("Тут нельзя делать action");
    }

    @Override
    public void setNextMenu(Menu nextMenu) {
        this.nextMenu = nextMenu;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
