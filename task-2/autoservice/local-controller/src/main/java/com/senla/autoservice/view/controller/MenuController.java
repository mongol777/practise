package com.senla.autoservice.view.controller;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.builder.MenuBuilder;
import com.senla.autoservice.view.navigator.MenuNavigator;

import java.util.Scanner;

public class MenuController {
    private final MenuNavigator menuNavigator;

    public MenuController(Scanner scanner, ServiceFactory serviceFactory) {
        MenuBuilder menuBuilder = new MenuBuilder(scanner, serviceFactory);
        menuBuilder.buildMenu();
        this.menuNavigator = new MenuNavigator(scanner, menuBuilder.getRootMenu());
    }

    public void run() {
        menuNavigator.printMenu();
    }
}
