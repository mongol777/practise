package com.senla.autoservice.view.menu.main;

import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.List;

public class MainMenu implements Menu {
    private List<MenuItem> menuItems;

    public MainMenu() {}

    @Override
    public String getTitle() {
        return "Главное меню";
    }

    @Override
    public List<MenuItem> getItems() {
        return menuItems;
    }

    @Override
    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
