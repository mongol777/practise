package com.senla.autoservice.view.menu_item.master;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.master.CreateMasterAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.Scanner;

public class CreateMasterItem implements MenuItem {
    private final Action action;
    private final Scanner scanner;

    public CreateMasterItem(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.action = new CreateMasterAction(scanner, serviceFactory);
    }

    public Scanner getScanner() {
        return scanner;
    }

    public String getTitle() {
        return "Создание мастера";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
