package com.senla.autoservice.view.action.place;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

public class ReadAllPlaceAction implements Action {
    private final PlaceService placeService;

    public ReadAllPlaceAction(ServiceFactory serviceFactory) {
        this.placeService = serviceFactory.getPlaceService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Все места: ");
        placeService.readAll().forEach(System.out::println);
        System.out.println("------------------------------");
    }
}
