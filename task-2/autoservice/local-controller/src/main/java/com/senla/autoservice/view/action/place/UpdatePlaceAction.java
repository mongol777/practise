package com.senla.autoservice.view.action.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Optional;
import java.util.Scanner;

public class UpdatePlaceAction implements Action {

    private final Scanner scanner;
    private final PlaceService placeService;
    private final GarageService garageService;
    private final OrderService orderService;

    public UpdatePlaceAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.placeService = serviceFactory.getPlaceService();
        this.garageService = serviceFactory.getGarageService();
        this.orderService = serviceFactory.getOrderService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Place oldPlace = findPlaceForUpdate();
        if (oldPlace != null) {
            Optional<Order> orderWithUpdatablePlaceOptional = orderService
                    .readAll()
                    .stream()
                    .filter(order -> order.getPlace().equals(oldPlace))
                    .findAny();
            if (orderWithUpdatablePlaceOptional.isPresent()) {
                orderWithUpdatablePlaceOptional.get().setPlace(null);
                updatePlace(oldPlace);
                orderWithUpdatablePlaceOptional.get().setPlace(oldPlace);
            } else {
                updatePlace(oldPlace);
            }
        } else {
            System.out.println("Место не найдено, попробуйте снова...");
        }
        System.out.println("------------------------------");
    }

    private void updatePlace(Place oldPlace) {
        Garage newPlaceGarage = chooseGarage(oldPlace);

        oldPlace.setGarage(newPlaceGarage);

        if (placeService.update(oldPlace.getId(), oldPlace)) {
            System.out.println("Место успешно обновлено!");
        } else {
            System.out.println("Не удалось обновить место по техническим причинам");
        }
    }

    private Garage chooseGarage(Place oldPlace) {
        System.out.println("Введите номер гаража, в который будем переводить место (0, если оставляем старый)");
        System.out.println("------------------------------");
        System.out.println("Все номера гаражей: ");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getId()));
        System.out.println("------------------------------");

        Garage newPlaceGarage;
        int newGarageId = Integer.parseInt(scanner.next());
        if (newGarageId != 0) {
            newPlaceGarage = garageService.read(newGarageId);
        } else {
            newPlaceGarage = oldPlace.getGarage();
        }
        return newPlaceGarage;
    }

    private Place findPlaceForUpdate() {
        System.out.println("Введите номер места, которое хотите изменить");
        int updatablePlaceId = Integer.parseInt(scanner.next());
        Place oldPlace = placeService.read(updatablePlaceId);
        System.out.println("------------------------------");

        return oldPlace;
    }
}
