package com.senla.autoservice.view.action.master;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Optional;
import java.util.Scanner;

public class UpdateMasterAction implements Action {
    private final Scanner scanner;
    private final MasterService masterService;
    private final GarageService garageService;
    private final OrderService orderService;

    public UpdateMasterAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.masterService = serviceFactory.getMasterService();
        this.garageService = serviceFactory.getGarageService();
        this.orderService = serviceFactory.getOrderService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Master oldMaster = findMasterForUpdate();
        if (oldMaster != null) {
            Optional<Order> orderWithUpdatableMasterOptional = orderService
                    .readAll()
                    .stream()
                    .filter(order -> order.getMasters().contains(oldMaster))
                    .findAny();
            if (orderWithUpdatableMasterOptional.isPresent()) {
                orderWithUpdatableMasterOptional.get().deleteMaster(oldMaster);
                updateMaster(oldMaster);
                orderWithUpdatableMasterOptional.get().addMaster(oldMaster);
            } else {
                updateMaster(oldMaster);
            }
        } else {
            System.out.println("Мастер не найден, попробуйте снова...");
        }
    }

    private void updateMaster(Master oldMaster) {
        System.out.println("Введите новое имя");
        String newMasterName = scanner.next();
        if (newMasterName == null) {
            newMasterName = oldMaster.getName();
        }

        System.out.println("------------------------------");

        Garage newMasterGarage = chooseGarage(oldMaster);

        oldMaster.setName(newMasterName);
        oldMaster.setGarage(newMasterGarage);

        if (masterService.update(oldMaster.getId(), oldMaster)) {
            System.out.println("Мастер успешно обновлён!");
        } else {
            System.out.println("Не удалось обновить мастера по техническим причинам");
        }
    }

    private Garage chooseGarage(Master oldMaster) {
        System.out.println("Введите номер гаража, в который будем переводить мастера (0, если оставляем старый)");
        System.out.println("------------------------------");
        System.out.println("Все номера гаражей: ");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getId()));
        System.out.println("------------------------------");

        Garage newMasterGarage;
        int newGarageId = Integer.parseInt(scanner.next());
        if (newGarageId != 0) {
            newMasterGarage = garageService.read(newGarageId);
        } else {
            newMasterGarage = oldMaster.getGarage();
        }
        return newMasterGarage;
    }

    private Master findMasterForUpdate() {
        System.out.println("Введите номер мастера, которого хотите изменить");
        int updatableMasterId = Integer.parseInt(scanner.next());
        Master oldMaster = masterService.read(updatableMasterId);
        System.out.println("------------------------------");

        return oldMaster;
    }
}
