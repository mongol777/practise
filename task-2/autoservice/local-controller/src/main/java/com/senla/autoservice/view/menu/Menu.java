package com.senla.autoservice.view.menu;

import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.List;

public interface Menu {
    String getTitle();
    List<MenuItem> getItems();
    void setMenuItems(List<MenuItem> menuItems);
}
