package com.senla.autoservice.view.navigator;

import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu.main.MainMenu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.Scanner;

public class MenuNavigator {
    private final Scanner scanner;
    private final Menu rootMenu;
    private Menu currentMenu;

    public MenuNavigator(Scanner scanner, Menu currentMenu) {
        this.scanner = scanner;
        this.currentMenu = currentMenu;
        this.rootMenu = currentMenu;
    }

    public void printMenu() {
        while (true) {
            int count = 1;
            System.out.println(currentMenu.getTitle());

            if (currentMenu instanceof MainMenu) {
                currentMenu.getItems().get(0).doAction();
                int numberOfNextMenu = currentMenu.getItems().get(0).getAction().getNumberOfNextMenu();

                if (numberOfNextMenu == 5) {
                    System.out.println("До свидания! Удачного дня!");
                    System.exit(0);
                }

                if (numberOfNextMenu > currentMenu.getItems().size() - 1 || numberOfNextMenu <= 0) {
                    System.out.println("Неверный пункт меню. Попробуйте снова...");
                    continue;
                }

                navigate(numberOfNextMenu);
                continue;
            }

            for (MenuItem menuItem : currentMenu.getItems()) {
                if (menuItem == null) {
                    continue;
                }
                System.out.println(count + " - " + menuItem.getTitle());
                count++;
            }

            if (count == currentMenu.getItems().size()) {
                System.out.println(count + " - На главное меню");
            }

            int numberOfMenuItem = scanner.nextInt();

            if (numberOfMenuItem == currentMenu.getItems().size()) {
                setCurrentMenu(rootMenu);
                continue;
            }

            if (numberOfMenuItem > currentMenu.getItems().size() || numberOfMenuItem <= 0) {
                System.out.println("Неверный пункт меню. Попробуйте снова...");
                continue;
            }

            currentMenu.getItems().get(numberOfMenuItem).doAction();
        }
    }

    private void navigate(Integer index) {
        setCurrentMenu(currentMenu.getItems().get(index).getNextMenu());
    }

    public void setCurrentMenu(Menu currentMenu) {
        this.currentMenu = currentMenu;
    }

}
