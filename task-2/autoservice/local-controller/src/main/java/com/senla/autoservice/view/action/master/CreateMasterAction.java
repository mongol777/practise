package com.senla.autoservice.view.action.master;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class CreateMasterAction implements Action {
    private final Scanner scanner;
    private final MasterService masterService;
    private final GarageService garageService;

    public CreateMasterAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.masterService = serviceFactory.getMasterService();
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Введите имя нового мастера");
        String name = scanner.next();

        Garage garage = chooseGarage();

        if (garage != null) {
            Master newMaster = new Master(name, garage);

            if (masterService.save(newMaster)) {
                System.out.println("Мастер добавлен к гаражу!");
            } else {
                System.out.println("Не удалось создать мастера по техническим причинам");
            }
        } else {
            System.out.println("Гараж не найден, мастер не может быть создан");
        }

        System.out.println("------------------------------");
    }

    private Garage chooseGarage() {
        System.out.println("Введите номер гаража, к которому будет добавлен мастер");
        System.out.println("------------------------------");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getId()));
        System.out.println("------------------------------");

        int garageId = scanner.nextInt();

        return garageService.read(garageId);
    }
}
