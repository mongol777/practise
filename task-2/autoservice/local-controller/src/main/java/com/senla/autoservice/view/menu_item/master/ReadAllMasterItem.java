package com.senla.autoservice.view.menu_item.master;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.master.ReadAllMasterAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

public class ReadAllMasterItem implements MenuItem {
    private final Action action;

    public ReadAllMasterItem(ServiceFactory serviceFactory) {
        this.action = new ReadAllMasterAction(serviceFactory);
    }

    public String getTitle() {
        return "Все мастера";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
