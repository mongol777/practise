package com.senla.autoservice.view.menu.order;

import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.List;

public class OrderMenu implements Menu {
    private List<MenuItem> menuItems;

    public OrderMenu() {}

    @Override
    public String getTitle() {
        return "Меню заказов";
    }

    @Override
    public List<MenuItem> getItems() {
        return menuItems;
    }

    @Override
    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
