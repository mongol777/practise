package com.senla.autoservice.view.action.place;

import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class DeletePlaceAction implements Action {
    private final Scanner scanner;
    private final PlaceService placeService;

    public DeletePlaceAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.placeService = serviceFactory.getPlaceService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Введите номер места, которое хотите удалить");
        int deletablePlaceId = Integer.parseInt(scanner.next());
        Place deletablePlace = placeService.read(deletablePlaceId);
        System.out.println("------------------------------");

        if (placeService.delete(deletablePlace.getId())) {
            System.out.println("Место успешно удалено!");
        } else {
            System.out.println("Не удалось удалить место по техническим причинам");
        }

        System.out.println("------------------------------");
    }
}
