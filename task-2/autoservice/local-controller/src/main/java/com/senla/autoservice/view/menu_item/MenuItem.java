package com.senla.autoservice.view.menu_item;

import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.menu.Menu;

public interface MenuItem {
    void doAction();
    String getTitle();
    Action getAction();
    void setNextMenu(Menu menu);
    Menu getNextMenu();
}
