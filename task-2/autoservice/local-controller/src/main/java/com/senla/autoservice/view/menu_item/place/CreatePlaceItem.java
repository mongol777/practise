package com.senla.autoservice.view.menu_item.place;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.place.CreatePlaceAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.Scanner;

public class CreatePlaceItem implements MenuItem {

    private final Action action;
    private final Scanner scanner;

    public CreatePlaceItem(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.action = new CreatePlaceAction(scanner, serviceFactory);
    }

    @Override
    public void doAction() {
        action.execute();
    }

    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public String getTitle() {
        return "Создание места";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
