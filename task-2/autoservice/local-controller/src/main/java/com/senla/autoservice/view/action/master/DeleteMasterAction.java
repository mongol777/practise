package com.senla.autoservice.view.action.master;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class DeleteMasterAction implements Action {

    private final Scanner scanner;
    private final MasterService masterService;

    public DeleteMasterAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.masterService = serviceFactory.getMasterService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Введите номер мастера, которого хотите удалить");
        int deletableMasterId = Integer.parseInt(scanner.next());
        Master deletableMaster = masterService.read(deletableMasterId);
        System.out.println("------------------------------");

        if (masterService.delete(deletableMaster.getId())) {
            System.out.println("Мастер успешно удалён!");
        } else {
            System.out.println("Не удалось удалить мастера по техническим причинам");
        }
        System.out.println("------------------------------");
    }
}
