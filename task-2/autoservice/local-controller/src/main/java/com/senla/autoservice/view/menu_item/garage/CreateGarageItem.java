package com.senla.autoservice.view.menu_item.garage;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.garage.CreateGarageAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

public class CreateGarageItem implements MenuItem {
    private final Action action;

    public CreateGarageItem(ServiceFactory serviceFactory) {
        this.action = new CreateGarageAction(serviceFactory);
    }

    public String getTitle() {
        return "Создание гаража";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
