package com.senla.autoservice.view.menu.place;

import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

import java.util.List;

public class PlaceMenu implements Menu {
    private List<MenuItem> menuItems;

    public PlaceMenu() {}

    @Override
    public String getTitle() {
        return "Меню мест";
    }

    @Override
    public List<MenuItem> getItems() {
        return menuItems;
    }

    @Override
    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
