package com.senla.autoservice.view.action.place;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.place.Place;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.place.PlaceService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class CreatePlaceAction implements Action {

    private final Scanner scanner;
    private final PlaceService placeService;
    private final GarageService garageService;

    public CreatePlaceAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.placeService = serviceFactory.getPlaceService();
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Garage garage = chooseGarage();

        if (garage != null) {
            Place newPlace = new Place(garage);
            if (placeService.save(newPlace)) {
                System.out.println("Место добавлено к гаражу!");
            } else {
                System.out.println("Не удалось создать место по техническим причинам");
            }
        } else {
            System.out.println("Гараж не найден, нельзя создать место");
        }

        System.out.println("------------------------------");
    }

    private Garage chooseGarage() {
        System.out.println("Введите номер гаража, к которому будет добавлено место");
        System.out.println("------------------------------");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getId()));
        System.out.println("------------------------------");

        int garageId = scanner.nextInt();

        System.out.println("------------------------------");

        return garageService.read(garageId);
    }
}
