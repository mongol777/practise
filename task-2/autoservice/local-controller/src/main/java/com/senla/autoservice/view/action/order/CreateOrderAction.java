package com.senla.autoservice.view.action.order;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.time.LocalDate;
import java.util.Scanner;

public class CreateOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final GarageService garageService;

    public CreateOrderAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.orderService = serviceFactory.getOrderService();
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Garage garage = chooseGarage();

        if (garage != null) {
            System.out.println("Введите цену заказа");
            double price = Double.parseDouble(scanner.next());
            System.out.println("------------------------------");

            System.out.println("Ввод дат производиться в следующем порядке: год, затем номер месяца, затем день");
            System.out.println("------------------------------");

            Order newOrder = new Order(createReceivingDate(), createExpirationDate(), createStartDate(), createEndDate(), price, garage);

            if (orderService.save(newOrder)) {
                System.out.println("Заказ добавлен к гаражу!");
            } else {
                System.out.println("Не удалось создать заказ по техническим причинам");
            }
        } else {
            System.out.println("Гараж не найден, заказ не может быть создан");
        }

        System.out.println("------------------------------");
    }

    private Garage chooseGarage() {
        System.out.println("Введите номер гаража, к которому будет добавлен заказ");
        System.out.println("------------------------------");
        garageService.readAll().forEach(garage -> System.out.println("Гараж №" + garage.getId()));
        System.out.println("------------------------------");

        int garageId = scanner.nextInt();

        return garageService.read(garageId);
    }

    private LocalDate createReceivingDate() {
        System.out.println("Введите дату получения заказа ");
        int yearOfReceiving = scanner.nextInt();
        int monthOfReceiving = scanner.nextInt();
        int dayOfReceiving = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfReceiving, monthOfReceiving, dayOfReceiving);
    }

    private LocalDate createExpirationDate() {
        System.out.println("Введите дату примерного окончания заказа ");
        int yearOfExpiration = scanner.nextInt();
        int monthOfExpiration = scanner.nextInt();
        int dayOfExpiration = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfExpiration, monthOfExpiration, dayOfExpiration);
    }

    private LocalDate createStartDate() {
        System.out.println("Введите дату начала выполнения заказа ");
        int yearOfStart = scanner.nextInt();
        int monthOfStart = scanner.nextInt();
        int dayOfStart = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfStart, monthOfStart, dayOfStart);
    }

    private LocalDate createEndDate() {
        System.out.println("Введите дату окончания выполнения заказа ");
        int yearOfEnd = scanner.nextInt();
        int monthOfEnd = scanner.nextInt();
        int dayOfEnd = scanner.nextInt();
        System.out.println("------------------------------");
        return LocalDate.of(yearOfEnd, monthOfEnd, dayOfEnd);
    }
}
