package com.senla.autoservice.view.menu_item.place;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.action.Action;
import com.senla.autoservice.view.action.place.ReadAllPlaceAction;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu_item.MenuItem;

public class ReadAllPlaceItem implements MenuItem {
    private final Action action;

    public ReadAllPlaceItem(ServiceFactory serviceFactory) {
        this.action = new ReadAllPlaceAction(serviceFactory);
    }

    @Override
    public void doAction() {
        action.execute();
    }

    @Override
    public String getTitle() {
        return "Все места";
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void setNextMenu(Menu menu) {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }

    @Override
    public Menu getNextMenu() {
        throw new RuntimeException("Это конечная точка, меню дальше нет.");
    }
}
