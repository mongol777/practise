package com.senla.autoservice.view.action.main;

import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class MainMenuAction implements Action {
    private final Scanner scanner;
    private int numberOfNextMenu;

    public MainMenuAction(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public int getNumberOfNextMenu() {
        return numberOfNextMenu;
    }

    @Override
    public void execute() {
        System.out.println("Добро пожаловать в Автосервис!");
        System.out.println("------------------------------");

        System.out.println("Выберите с чем будете работать: ");
        System.out.println("------------------------------");

        System.out.println("1 - Мастера");
        System.out.println("2 - Места");
        System.out.println("3 - Заказы");
        System.out.println("4 - Гаражи");
        System.out.println("5 - Выход из приложения");
        System.out.println("------------------------------");

        this.numberOfNextMenu = Integer.parseInt(scanner.next());
    }
}
