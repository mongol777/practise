package com.senla.autoservice.view.action;

public interface Action {
    int getNumberOfNextMenu();
    void execute();
}
