package com.senla.autoservice.view.action.order;

import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.models.order.status.OrderStatus;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class DeleteOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;

    public DeleteOrderAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.orderService = serviceFactory.getOrderService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Введите номер заказа, которого хотите удалить");
        int deletableOrderId = Integer.parseInt(scanner.next());
        Order deletableOrder = orderService.read(deletableOrderId);
        System.out.println("------------------------------");

        if (deletableOrder != null) {
            System.out.println("Введите новый статус заказа");
            System.out.println("------------------------------");
            System.out.println("В - выполненный, О - отменённый, У - удалённый");
            System.out.println("------------------------------");
            String newOrderStatus = scanner.next();
            switch (newOrderStatus) {
                case "В":
                    deletableOrder.setStatus(OrderStatus.DONE);
                    break;
                case "О":
                    deletableOrder.setStatus(OrderStatus.CANCELED);
                    break;
                case "У":
                    deletableOrder.setStatus(OrderStatus.DELETED);
                    break;
            }

            if (orderService.update(deletableOrder.getId(), deletableOrder)) {
                System.out.println("Заказ успешно переведён в другой статус и сохранён в базе");
            } else {
                System.out.println("Не удалось перевести заказ в другой статус по техническим причинам");
            }
        } else {
            System.out.println("Заказ не найден, попробуйте снова...");
        }
        System.out.println("------------------------------");
    }
}
