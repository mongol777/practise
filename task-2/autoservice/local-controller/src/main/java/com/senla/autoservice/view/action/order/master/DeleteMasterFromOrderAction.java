package com.senla.autoservice.view.action.order.master;

import com.senla.autoservice.models.master.Master;
import com.senla.autoservice.models.order.Order;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.service.order.OrderService;
import com.senla.autoservice.view.action.Action;

import java.util.Scanner;

public class DeleteMasterFromOrderAction implements Action {
    private final Scanner scanner;
    private final OrderService orderService;
    private final MasterService masterService;

    public DeleteMasterFromOrderAction(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.orderService = serviceFactory.getOrderService();
        this.masterService = serviceFactory.getMasterService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        Order order = findOrderForDeleteMaster();
        if (order != null) {
            Master master = chooseMaster(order);

            if (master != null) {
                order.deleteMaster(master);
                if (orderService.update(order.getId(), order)) {
                    System.out.println("Мастер успешно удалён из заказа!");
                } else {
                    System.out.println("Не удалось обновить заказ по техническим причинам");
                }
            } else {
                System.out.println("Мастер не найден. Повторите попытку...");
            }
        } else {
            System.out.println("Заказ не найден, повторите попытку...");
        }
        System.out.println("------------------------------");
    }

    private Master chooseMaster(Order order) {
        System.out.println("Введите номер мастера, которого исключать из заказа");
        System.out.println("------------------------------");
        System.out.println(order.getMasters());
        System.out.println("------------------------------");
        int masterId = scanner.nextInt();
        System.out.println("------------------------------");

        return masterService.read(masterId);
    }


    private Order findOrderForDeleteMaster() {
        System.out.println("Введите номер заказа, из которого хотите исключить мастера");
        int updatableOrderId = Integer.parseInt(scanner.next());
        Order order = orderService.read(updatableOrderId);
        System.out.println("------------------------------");

        return order;
    }
}
