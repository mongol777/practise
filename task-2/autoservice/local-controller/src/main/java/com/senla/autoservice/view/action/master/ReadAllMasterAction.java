package com.senla.autoservice.view.action.master;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.master.MasterService;
import com.senla.autoservice.view.action.Action;

public class ReadAllMasterAction implements Action {
    private final MasterService masterService;

    public ReadAllMasterAction(ServiceFactory serviceFactory) {
        this.masterService = serviceFactory.getMasterService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        System.out.println("Все мастера: ");
        masterService.readAll().forEach(System.out::println);
        System.out.println("------------------------------");
    }
}
