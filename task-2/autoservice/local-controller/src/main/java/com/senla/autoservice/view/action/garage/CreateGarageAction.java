package com.senla.autoservice.view.action.garage;

import com.senla.autoservice.models.garage.Garage;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.service.garage.GarageService;
import com.senla.autoservice.view.action.Action;

public class CreateGarageAction implements Action {
    private final GarageService garageService;

    public CreateGarageAction(ServiceFactory serviceFactory) {
        this.garageService = serviceFactory.getGarageService();
    }

    @Override
    public int getNumberOfNextMenu() {
        throw new RuntimeException("В Action нельзя получить номер следующего меню");
    }

    @Override
    public void execute() {
        if (garageService.save(new Garage())) {
            System.out.println("Гараж создан!");
        } else {
            System.out.println("Не удалось создать гараж по техническим причинам");
        }
        System.out.println("------------------------------");
    }
}
