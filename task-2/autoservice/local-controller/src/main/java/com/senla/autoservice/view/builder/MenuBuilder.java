package com.senla.autoservice.view.builder;

import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.menu.garage.GarageMenu;
import com.senla.autoservice.view.menu.main.MainMenu;
import com.senla.autoservice.view.menu.master.MasterMenu;
import com.senla.autoservice.view.menu.Menu;
import com.senla.autoservice.view.menu.order.OrderMenu;
import com.senla.autoservice.view.menu.place.PlaceMenu;
import com.senla.autoservice.view.menu_item.garage.CreateGarageItem;
import com.senla.autoservice.view.menu_item.garage.DeleteGarageItem;
import com.senla.autoservice.view.menu_item.garage.MainGarageMenuItem;
import com.senla.autoservice.view.menu_item.garage.ReadAllGarageItem;
import com.senla.autoservice.view.menu_item.main.MainMenuItem;
import com.senla.autoservice.view.menu_item.master.CreateMasterItem;
import com.senla.autoservice.view.menu_item.master.DeleteMasterItem;
import com.senla.autoservice.view.menu_item.master.MainMasterMenuItem;
import com.senla.autoservice.view.menu_item.MenuItem;
import com.senla.autoservice.view.menu_item.master.ReadAllMasterItem;
import com.senla.autoservice.view.menu_item.master.UpdateMasterItem;
import com.senla.autoservice.view.menu_item.order.master.AddMasterToOrderItem;
import com.senla.autoservice.view.menu_item.order.CreateOrderItem;
import com.senla.autoservice.view.menu_item.order.DeleteOrderItem;
import com.senla.autoservice.view.menu_item.order.MainOrderMenuItem;
import com.senla.autoservice.view.menu_item.order.ReadAllOrderItem;
import com.senla.autoservice.view.menu_item.order.UpdateOrderItem;
import com.senla.autoservice.view.menu_item.order.master.DeleteMasterFromOrderItem;
import com.senla.autoservice.view.menu_item.order.place.AddPlaceToOrderItem;
import com.senla.autoservice.view.menu_item.order.place.DeletePlaceFromOrderItem;
import com.senla.autoservice.view.menu_item.place.CreatePlaceItem;
import com.senla.autoservice.view.menu_item.place.DeletePlaceItem;
import com.senla.autoservice.view.menu_item.place.MainPlaceMenuItem;
import com.senla.autoservice.view.menu_item.place.ReadAllPlaceItem;
import com.senla.autoservice.view.menu_item.place.UpdatePlaceItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuBuilder {
    private Menu rootMenu;
    private final Scanner scanner;
    private final ServiceFactory serviceFactory;

    public MenuBuilder(Scanner scanner, ServiceFactory serviceFactory) {
        this.scanner = scanner;
        this.serviceFactory = serviceFactory;
    }

    public Menu getRootMenu() {
        return rootMenu;
    }

    public void buildMenu() {
        List<MenuItem> mainMenuItems = new ArrayList<>();

        MenuItem mainMenuItem = new MainMenuItem(scanner);
        mainMenuItems.add(mainMenuItem);

        buildMasterMenu(mainMenuItems);
        buildPlaceMenu(mainMenuItems);
        buildOrderMenu(mainMenuItems);
        buildGarageMenu(mainMenuItems);

        MainMenu mainMenu = new MainMenu();
        mainMenu.setMenuItems(mainMenuItems);

        rootMenu = mainMenu;
    }

    private void buildMasterMenu(List<MenuItem> mainMenuItems) {
        Menu masterMenu = new MasterMenu();
        MenuItem masterMenuItem = new MainMasterMenuItem();

        List<MenuItem> masterMenuItems = new ArrayList<>();

        masterMenuItems.add(null);
        masterMenuItems.add(new CreateMasterItem(scanner, serviceFactory));
        masterMenuItems.add(new ReadAllMasterItem(serviceFactory));
        masterMenuItems.add(new UpdateMasterItem(scanner, serviceFactory));
        masterMenuItems.add(new DeleteMasterItem(scanner, serviceFactory));

        masterMenu.setMenuItems(masterMenuItems);

        masterMenuItem.setNextMenu(masterMenu);

        mainMenuItems.add(masterMenuItem);
    }

    private void buildPlaceMenu(List<MenuItem> mainMenuItems) {
        Menu placeMenu = new PlaceMenu();
        MenuItem placeMenuItem = new MainPlaceMenuItem();

        List<MenuItem> placeMenuItems = new ArrayList<>();

        placeMenuItems.add(null);
        placeMenuItems.add(new CreatePlaceItem(scanner, serviceFactory));
        placeMenuItems.add(new ReadAllPlaceItem(serviceFactory));
        placeMenuItems.add(new UpdatePlaceItem(scanner, serviceFactory));
        placeMenuItems.add(new DeletePlaceItem(scanner, serviceFactory));

        placeMenu.setMenuItems(placeMenuItems);

        placeMenuItem.setNextMenu(placeMenu);

        mainMenuItems.add(placeMenuItem);
    }

    private void buildOrderMenu(List<MenuItem> mainMenuItems) {
        Menu orderMenu = new OrderMenu();
        MenuItem orderMenuItem = new MainOrderMenuItem();

        List<MenuItem> orderMenuItems = new ArrayList<>();

        orderMenuItems.add(null);
        orderMenuItems.add(new CreateOrderItem(scanner, serviceFactory));
        orderMenuItems.add(new ReadAllOrderItem(serviceFactory));
        orderMenuItems.add(new UpdateOrderItem(scanner, serviceFactory));
        orderMenuItems.add(new DeleteOrderItem(scanner, serviceFactory));
        orderMenuItems.add(new AddMasterToOrderItem(scanner, serviceFactory));
        orderMenuItems.add(new DeleteMasterFromOrderItem(scanner, serviceFactory));
        orderMenuItems.add(new AddPlaceToOrderItem(scanner, serviceFactory));
        orderMenuItems.add(new DeletePlaceFromOrderItem(scanner, serviceFactory));

        orderMenu.setMenuItems(orderMenuItems);

        orderMenuItem.setNextMenu(orderMenu);

        mainMenuItems.add(orderMenuItem);
    }

    private void buildGarageMenu(List<MenuItem> mainMenuItems) {
        Menu garageMenu = new GarageMenu();
        MenuItem garageMenuItem = new MainGarageMenuItem();

        List<MenuItem> garageMenuItems = new ArrayList<>();

        garageMenuItems.add(null);
        garageMenuItems.add(new CreateGarageItem(serviceFactory));
        garageMenuItems.add(new ReadAllGarageItem(serviceFactory));
        garageMenuItems.add(new DeleteGarageItem(scanner, serviceFactory));

        garageMenu.setMenuItems(garageMenuItems);

        garageMenuItem.setNextMenu(garageMenu);

        mainMenuItems.add(garageMenuItem);
    }
}
