import com.senla.autoservice.service.factory.CommonServiceFactory;
import com.senla.autoservice.service.factory.ServiceFactory;
import com.senla.autoservice.view.controller.MenuController;

import java.util.Scanner;

public class Autoservice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ServiceFactory serviceFactory = new CommonServiceFactory();
        MenuController controller = new MenuController(scanner, serviceFactory);

        controller.run();
    }
}
