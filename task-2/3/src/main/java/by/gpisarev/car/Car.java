package by.gpisarev.car;

import by.gpisarev.parts.Body;
import by.gpisarev.parts.CarPart;
import by.gpisarev.parts.Chassis;
import by.gpisarev.parts.Engine;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car implements Product {

    private CarPart engine;
    private CarPart chassis;
    private CarPart body;

    @Override
    public void installFirstPart(CarPart engine) {
        this.engine = engine;
    }

    @Override
    public void installSecondPart(CarPart body) {
        this.body = body;
    }

    @Override
    public void installThirdPart(CarPart chassis) {
        this.chassis = chassis;
    }
}
