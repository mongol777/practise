package by.gpisarev.car;

import by.gpisarev.parts.CarPart;

public interface Product {
    void installFirstPart(CarPart engine);
    void installSecondPart(CarPart body);
    void installThirdPart(CarPart chassis);
}
