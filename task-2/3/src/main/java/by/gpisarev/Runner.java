package by.gpisarev;

import by.gpisarev.car.Car;
import by.gpisarev.part_builder.FirstPartBuilder;
import by.gpisarev.part_builder.ThirdPartBuilder;
import by.gpisarev.part_builder.SecondPartBuilder;
import by.gpisarev.product_builder.CarBuilderImpl;

public class Runner {
    public static void main(String[] args) {

        System.out.println(new CarBuilderImpl(new FirstPartBuilder(),
                                              new SecondPartBuilder(),
                                              new ThirdPartBuilder())
                .assembleProduct(new Car()));

    }
}
