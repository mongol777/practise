package by.gpisarev.product_builder;

import by.gpisarev.car.Product;

public interface CarBuilder {
    Product assembleProduct(Product product);
}
