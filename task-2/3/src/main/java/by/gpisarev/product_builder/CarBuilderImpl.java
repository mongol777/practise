package by.gpisarev.product_builder;

import by.gpisarev.car.Product;
import by.gpisarev.part_builder.PartBuilder;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CarBuilderImpl implements CarBuilder {

    private final PartBuilder engineBuilder;
    private final PartBuilder bodyBuilder;
    private final PartBuilder chassisBuilder;

    @Override
    public Product assembleProduct(Product product) {

        product.installFirstPart(engineBuilder.buildCarPart());
        product.installSecondPart(bodyBuilder.buildCarPart());
        product.installThirdPart(chassisBuilder.buildCarPart());

        return product;
    }
}
