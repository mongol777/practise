package by.gpisarev.parts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Chassis implements CarPart {
    private long id;
    private long length;
}
