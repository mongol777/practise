package by.gpisarev.parts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Engine implements CarPart {
    private long id;
    private String type;
    private int power;
}
