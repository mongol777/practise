package by.gpisarev.part_builder;

import by.gpisarev.parts.Body;
import by.gpisarev.parts.CarPart;

public class FirstPartBuilder implements PartBuilder {
    // мне кажется сюда должны приходить параметры из Runner класса
    @Override
    public CarPart buildCarPart() {
        return new Body(1, "Universal");
    }
}
