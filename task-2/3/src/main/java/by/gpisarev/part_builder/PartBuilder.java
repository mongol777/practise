package by.gpisarev.part_builder;

import by.gpisarev.parts.CarPart;

public interface PartBuilder {
    CarPart buildCarPart();
}
