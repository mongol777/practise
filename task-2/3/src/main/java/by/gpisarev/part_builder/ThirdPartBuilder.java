package by.gpisarev.part_builder;

import by.gpisarev.parts.CarPart;
import by.gpisarev.parts.Chassis;

public class ThirdPartBuilder implements PartBuilder {
    // мне кажется сюда должны приходить параметры из Runner класса
    @Override
    public CarPart buildCarPart() {
        return new Chassis(1, 100);
    }
}
