package by.gpisarev.part_builder;

import by.gpisarev.parts.CarPart;
import by.gpisarev.parts.Engine;

public class SecondPartBuilder implements PartBuilder {
    // мне кажется сюда должны приходить параметры из Runner класса
    @Override
    public CarPart buildCarPart() {
        return new Engine(1, "Petrol", 90);
    }
}
