package by.gpisarev.first;

import java.util.Random;

public class Runner {
    public static void main(String[] args) {
        int randomNumber = new Random().nextInt(999);
        System.out.println("Random number (from 1 to 999): " + randomNumber);

        char[] randomNumberChar = String.valueOf(randomNumber).toCharArray();
        char maxNumber = randomNumberChar[0];

        for (char number : randomNumberChar) {
            if (number > maxNumber) {
                maxNumber = number;
            }
        }

        System.out.println("Max number is " + maxNumber);
    }
}
