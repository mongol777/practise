package by.gpisarev.models.counter;

import by.gpisarev.models.accessaries.Accessory;
import by.gpisarev.models.bouquet.Bouquet;
import by.gpisarev.models.flowers.Flower;

import java.util.List;

public class Counter {
    public static int countTotalPrice(Bouquet bouquet) {
        return countFlowerPrice(bouquet.getFlowers()) + countAccessoriesPrice(bouquet.getAccessories());
    }

    private static int countFlowerPrice(List<Flower> flowers) {
        int totalPriceFlowers = 0;
        for (Flower flower : flowers) {
            totalPriceFlowers = totalPriceFlowers + flower.getPrice();
        }
        return totalPriceFlowers;
    }

    private static int countAccessoriesPrice(List<Accessory> accessories) {
        int totalPriceAccessories = 0;
        for (Accessory accessory : accessories) {
            totalPriceAccessories = totalPriceAccessories + accessory.getPrice();
        }
        return totalPriceAccessories;
    }
}
