package by.gpisarev.models.factory;

import by.gpisarev.models.accessaries.Accessory;
import by.gpisarev.models.accessaries.FlowerWrapper;
import by.gpisarev.models.accessaries.FlowerWrapperColor;

import java.util.ArrayList;
import java.util.List;

public class AccessoryFactory {
    private static final List<Accessory> ACCESSORIES = new ArrayList<>();
    public static void buildAccessories() {
        Accessory wrapperFlower = new FlowerWrapper()
                .withName("Обёртка для цветов (зеленая)")
                .withColor(FlowerWrapperColor.GREEN)
                .withPrice(4);
        ACCESSORIES.add(wrapperFlower);
    }

    public static List<Accessory> getAccessories() {
        return ACCESSORIES;
    }
}
