package by.gpisarev.models.start;

import by.gpisarev.models.accessaries.Accessory;
import by.gpisarev.models.bouquet.Bouquet;
import by.gpisarev.models.counter.Counter;
import by.gpisarev.models.factory.AccessoryFactory;
import by.gpisarev.models.factory.BouquetFactory;
import by.gpisarev.models.factory.FlowerFactory;
import by.gpisarev.models.flowers.Flower;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        FlowerFactory.buildFlowers();
        List<Flower> flowers = FlowerFactory.getFlowers();

        AccessoryFactory.buildAccessories();
        List<Accessory> accessories = AccessoryFactory.getAccessories();

        BouquetFactory.buildBouquet(flowers, accessories);
        Bouquet bouquet = BouquetFactory.getBouquet();

        System.out.println("Цена букета: " + Counter.countTotalPrice(bouquet));

        // Вывод букета

        System.out.println(bouquet);
        System.out.println();

        // Операции с цветами и аксессуарами

       // bouquet.removeFlower(roseRed);

       // System.out.println(bouquet);
       // System.out.println();

       // bouquet.addAccessory(new FlowerWrapper()
       //         .withName("Обертка для цветов (красная)")
       //         .withColor(FlowerWrapperColor.RED)
       //         .withPrice(5));

       // System.out.println(bouquet);

        // Сортировка по уровню свежести

        //Stream<Flower> flowerStream = bouquet.getFlowers()
        // .stream()
        // .sorted(Comparator.comparingInt(flower -> flower.getFreshness().getValue()));

        //System.out.println(flowerStream.collect(Collectors.toList()));

        // Длина стебля в заданном диапазоне

        //bouquet.getFlowers().stream()
        //        .filter(flower -> flower.getStemLength() > 30 && flower.getStemLength() < 51)
        //        .collect(Collectors.toList())
        //        .forEach(System.out::println);
    }
}
