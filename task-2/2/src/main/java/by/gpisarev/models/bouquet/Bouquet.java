package by.gpisarev.models.bouquet;

import by.gpisarev.models.accessaries.Accessory;
import by.gpisarev.models.flowers.Flower;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class Bouquet {
    private List<Flower> flowers;
    private List<Accessory> accessories;

    public Bouquet(List<Flower> flowers, List<Accessory> accessories) {
        this.flowers = flowers;
        this.accessories = accessories;
    }

    public Bouquet withFlowers(List<Flower> flowers) {
        setFlowers(flowers);
        return this;
    }

    public Bouquet withAccessories(List<Accessory> accessories) {
        setAccessories(accessories);
        return this;
    }

    public Bouquet addFlower(Flower flower) {
        flowers.add(flower);
        return this;
    }

    public Bouquet removeFlower(Flower flower) {
        flowers.remove(flower);
        return this;
    }

    public Bouquet addAccessory(Accessory accessory) {
        accessories.add(accessory);
        return this;
    }

    public Bouquet removeAccessory(Accessory accessory) {
        accessories.remove(accessory);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bouquet bouquet = (Bouquet) o;
        return flowers.equals(bouquet.flowers) && accessories.equals(bouquet.accessories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flowers, accessories);
    }

    @Override
    public String toString() {
        String result = "";
        result = "Список цветов: " + flowers.toString() + ".\n Список аксессуаров: " + accessories.toString() + ".\n";
        return result;
    }
}
