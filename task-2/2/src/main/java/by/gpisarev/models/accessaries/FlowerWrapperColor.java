package by.gpisarev.models.accessaries;

import lombok.Getter;

@Getter
public enum FlowerWrapperColor {
    YELLOW("Жёлтый"),
    RED("Красный"),
    WHITE("Белый"),
    BLUE("Голубой"),
    GREEN("Зелёный");

    private final String color;

    FlowerWrapperColor(String color) {
        this.color = color;
    }
}
