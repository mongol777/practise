package by.gpisarev.models.flowers.secondary;

import lombok.Getter;

@Getter
public enum FlowerFreshness {
    FRESH(3),
    FADE(2),
    SEAR(1);

    private final int value;

    FlowerFreshness(int value) {
        this.value = value;
    }
}
