package by.gpisarev.models.factory;

import by.gpisarev.models.flowers.Flower;
import by.gpisarev.models.flowers.secondary.FlowerColor;
import by.gpisarev.models.flowers.secondary.FlowerFreshness;

import java.util.ArrayList;
import java.util.List;

public class FlowerFactory {
    private static final List<Flower> FLOWERS = new ArrayList<>();
    public static void buildFlowers() {
        Flower roseRed = new Flower()
                .withName("Роза")
                .withColor(FlowerColor.RED)
                .withFreshness(FlowerFreshness.FADE)
                .withStemLength(30)
                .withPrice(10);

        Flower roseBlue = new Flower()
                .withName("Роза")
                .withColor(FlowerColor.BLUE)
                .withFreshness(FlowerFreshness.SEAR)
                .withStemLength(50)
                .withPrice(10);

        Flower roseWhite = new Flower()
                .withName("Роза")
                .withColor(FlowerColor.WHITE)
                .withFreshness(FlowerFreshness.FRESH)
                .withStemLength(40)
                .withPrice(10);
        FLOWERS.add(roseRed);
        FLOWERS.add(roseBlue);
        FLOWERS.add(roseWhite);
    }

    public static List<Flower> getFlowers() {
        return FLOWERS;
    }
}
