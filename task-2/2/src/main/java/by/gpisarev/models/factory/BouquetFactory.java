package by.gpisarev.models.factory;

import by.gpisarev.models.accessaries.Accessory;
import by.gpisarev.models.bouquet.Bouquet;
import by.gpisarev.models.flowers.Flower;

import java.util.ArrayList;
import java.util.List;

public class BouquetFactory {
    private static Bouquet bouquet;

    public static void buildBouquet(List<Flower> flowers, List<Accessory> accessories) {
        bouquet = new Bouquet()
                .withFlowers(new ArrayList<>(flowers))
                .withAccessories(new ArrayList<>(accessories));
    }

    public static Bouquet getBouquet() {
        return bouquet;
    }
}
