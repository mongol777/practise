package by.gpisarev.models.flowers;

import by.gpisarev.models.flowers.secondary.FlowerColor;
import by.gpisarev.models.flowers.secondary.FlowerFreshness;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flower {
    protected int price;
    protected String name;
    protected FlowerColor color;
    protected FlowerFreshness freshness;
    protected int stemLength;

    public Flower withName(String name) {
        setName(name);
        return this;
    }

    public Flower withPrice(int price) {
        setPrice(price);
        return this;
    }

    public Flower withColor(FlowerColor color) {
        setColor(color);
        return this;
    }

    public Flower withFreshness(FlowerFreshness freshness) {
        setFreshness(freshness);
        return this;
    }

    public Flower withStemLength(int stemLength) {
        setStemLength(stemLength);
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return price == flower.price && stemLength == flower.stemLength && name.equals(flower.name) && color == flower.color && freshness == flower.freshness;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name, color, freshness, stemLength);
    }

    @Override
    public String toString() {
        return "Цветок: " + name +
                ", цена (за 1 шт.) " + price +
                ", цвет: " + color.getColor() +
                ", состояние свежести: " + freshness.getValue() +
                ", длина стебля: " + stemLength;
    }
}
