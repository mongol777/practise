package by.gpisarev.models.flowers.secondary;

import lombok.Getter;

@Getter
public enum FlowerColor {
    YELLOW("Жёлтый"),
    RED("Красный"),
    WHITE("Белый"),
    BLUE("Голубой");

    private final String color;

    FlowerColor(String color) {
        this.color = color;
    }
}
