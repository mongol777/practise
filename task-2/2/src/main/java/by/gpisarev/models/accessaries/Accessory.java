package by.gpisarev.models.accessaries;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Accessory {
    private String name;
    private int price;

    public Accessory withName(String name) {
        setName(name);
        return this;
    }

    public Accessory withPrice(int price) {
        setPrice(price);
        return this;
    }

    public abstract Accessory withColor(FlowerWrapperColor color);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Accessory accessory = (Accessory) o;
        return price == accessory.price && name.equals(accessory.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

    @Override
    public String toString() {
        return "Аксессуар: " + name + ", цена: " + price;
    }
}
