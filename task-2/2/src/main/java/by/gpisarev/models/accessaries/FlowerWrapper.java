package by.gpisarev.models.accessaries;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class FlowerWrapper extends Accessory {
    private FlowerWrapperColor color;

    public FlowerWrapper withColor(FlowerWrapperColor color) {
        setColor(color);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FlowerWrapper that = (FlowerWrapper) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    @Override
    public String toString() {
        return super.toString() + " , цвет: " + color.getColor();
    }
}
